package br.com.milo.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum Month {
	JANUARY(1),
	FEBRUARY(2),
	MARCH(3),
	APRIL(4),
	MAY(5),
	JUNE(6),
	JULY(7),
	AUGUST(8),
	SEPTEMBER(9),
	OCTOBER(10),
	NOVEMBER(11),
	DECEMBER(12);
	
	private int month;
	
	private Month(int month){
		this.month = month;
	}
	
	public int toInt(){
		return this.month;
	}
	
	
	private static final Map<Integer, Month> intToTypeMap = new HashMap<Integer, Month>();
	static {
	    for (Month type : Month.values()) {
	        intToTypeMap.put(type.toInt(), type);
	    }
	}

	public static Month fromInt(int i) {
	    Month type = intToTypeMap.get(Integer.valueOf(i));
	    if (type == null) 
	        return null;
	    return type;
	}
}
