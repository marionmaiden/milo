package br.com.milo.enumeration;

public enum UserType {
	ADMIN("ADMIN"),
	USER("USER");
	
	private String type;
	
	private UserType(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}
	
	public static UserType fromString(String type){
		return UserType.valueOf(type);
	}
}
