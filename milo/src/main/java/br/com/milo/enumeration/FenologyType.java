package br.com.milo.enumeration;

public enum FenologyType {
	VE("Emergência VE"),
	V1("Vegetativo V1"),
	V2("Vegetativo V2"),
	V3("Vegetativo V3"),
	V4("Vegetativo V4"),
	V5("Vegetativo V5"),
	V6("Vegetativo V6"),
	V7("Vegetativo V7"),
	V8("Vegetativo V8"),
	V9("Vegetativo V9"),
	V10("Vegetativo V10"),
	V11("Vegetativo V11"),
	V12("Vegetativo V12"),
	V13("Vegetativo V13"),
	V14("Vegetativo V14"),
	V15("Vegetativo V15"),
	V16("Vegetativo V16"),
	V17("Vegetativo V17"),
	V18("Vegetativo V18"),
	VT("Pendoamento VT"),
	R1("Reprodutivo R1"),
	R2("Reprodutivo R2"),
	R3("Reprodutivo R3"),
	R4("Reprodutivo R4"),
	R5("Reprodutivo R5"),
	R6("Reprodutivo R6"),
	R7("Reprodutivo R7"),
	R8("Reprodutivo R8"),
	R9("Reprodutivo R9");
	
	private String desc;
	
	private FenologyType(String desc){
		this.desc = desc;
	}
	
	public String getDesc(){
		return this.desc;
	}
	
	public FenologyType fromString(String str){
		return FenologyType.valueOf(str);
	}
	
}
