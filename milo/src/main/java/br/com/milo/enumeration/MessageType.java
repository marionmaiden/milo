package br.com.milo.enumeration;

public enum MessageType {

	ERROR("alert alert-dismissible alert-danger"),
	WARNING("alert alert-dismissible alert-warning"),
	SUCCESS("alert alert-dismissible alert-success"),
	INFO("alert alert-dismissible alert-info");
	
	String css;
	
	private MessageType(String css){
		this.css = css;
	}
	
	public String value(){
		return css;
	}
	
	
}
