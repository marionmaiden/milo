package br.com.milo.enumeration;

public enum TillageType {

	CORN("CORN","corn","milho"),
	SOY("SOY","soy", "soja"),
	WHEAT("WHEAT","wheat","trigo");
	
	private String type;
	private String en;
	private String pt;
	
	private TillageType(String type, String en, String pt){
		this.type = type;
		this.en = en;
		this.pt = pt;
	}

	public String getType() {
		return type;
	}

	public String getEn() {
		return en;
	}

	public String getPt() {
		return pt;
	}
	
	public static TillageType fromString(String str) {
        return valueOf(str);
    }
	
}
