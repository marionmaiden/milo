
package br.com.milo.domain.DTO;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "plants",
    "herbs",
    "plague",
    "disease",
//    "rain",
    "production",
    "gps"
})
public class Array {

    @JsonProperty("plants")
    private String plants;
    @JsonProperty("herbs")
    private boolean herbs;
    @JsonProperty("plague")
    private String plague;
    @JsonProperty("disease")
    private boolean disease;
//    @JsonProperty("rain")
//    private String rain;
    @JsonProperty("production")
    private String production;
    @JsonProperty("gps")
    private String gps;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Array() {
    }

    /**
     * 
     * @param plague
     * @param herbs
     * @param plants
     * @param gps
     * @param disease
     * @param production
     * @param rain
     */
    public Array(String plants, boolean herbs, String plague, boolean disease, String production, String gps) {
        this.plants = plants;
        this.herbs = herbs;
        this.plague = plague;
        this.disease = disease;
//        this.rain = rain;
        this.production = production;
        this.gps = gps;
    }

    /**
     * 
     * @return
     *     The plants
     */
    @JsonProperty("plants")
    public String getPlants() {
        return plants;
    }

    /**
     * 
     * @param plants
     *     The plants
     */
    @JsonProperty("plants")
    public void setPlants(String plants) {
        this.plants = plants;
    }

    /**
     * 
     * @return
     *     The herbs
     */
    @JsonProperty("herbs")
    public boolean isHerbs() {
        return herbs;
    }

    /**
     * 
     * @param herbs
     *     The herbs
     */
    @JsonProperty("herbs")
    public void setHerbs(boolean herbs) {
        this.herbs = herbs;
    }

    /**
     * 
     * @return
     *     The plague
     */
    @JsonProperty("plague")
    public String getPlague() {
        return plague;
    }

    /**
     * 
     * @param plague
     *     The plague
     */
    @JsonProperty("plague")
    public void setPlague(String plague) {
        this.plague = plague;
    }

    /**
     * 
     * @return
     *     The disease
     */
    @JsonProperty("disease")
    public boolean isDisease() {
        return disease;
    }

    /**
     * 
     * @param disease
     *     The disease
     */
    @JsonProperty("disease")
    public void setDisease(boolean disease) {
        this.disease = disease;
    }

    /**
     * 
     * @return
     *     The rain
     */
//    @JsonProperty("rain")
//    public String getRain() {
//        return rain;
//    }

    /**
     * 
     * @param rain
     *     The rain
     */
//    @JsonProperty("rain")
//    public void setRain(String rain) {
//        this.rain = rain;
//    }

    /**
     * 
     * @return
     *     The production
     */
    @JsonProperty("production")
    public String getProduction() {
        return production;
    }

    /**
     * 
     * @param production
     *     The production
     */
    @JsonProperty("production")
    public void setProduction(String production) {
        this.production = production;
    }

    /**
     * 
     * @return
     *     The gps
     */
    @JsonProperty("gps")
    public String getGps() {
        return gps;
    }

    /**
     * 
     * @param gps
     *     The gps
     */
    @JsonProperty("gps")
    public void setGps(String gps) {
        this.gps = gps;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
