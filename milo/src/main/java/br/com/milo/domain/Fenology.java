package br.com.milo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.milo.enumeration.FenologyType;
import br.com.milo.enumeration.TillageType;

@Entity
@Table(name = "fenology")
public class Fenology implements Serializable {

	private static final long serialVersionUID = 714006989890638856L;

	@Id
	@GeneratedValue(generator = "fenology_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "fenology_id_seq", sequenceName = "fenology_id_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name = "til_type")
	@Enumerated(EnumType.STRING)
	private TillageType type;

	@Column(name = "name")
	private String name;

	@Column(name = "fenology_type")
	@Enumerated(EnumType.STRING)
	private FenologyType fenologyType;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TillageType getType() {
		return type;
	}

	public void setType(TillageType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FenologyType getFenologyType() {
		return fenologyType;
	}

	public void setFenologyType(FenologyType fenologyType) {
		this.fenologyType = fenologyType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fenology other = (Fenology) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
