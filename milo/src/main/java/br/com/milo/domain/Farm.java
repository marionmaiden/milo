package br.com.milo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "farm")
public class Farm implements Serializable {

	private static final long serialVersionUID = 3605331584324240290L;

	/**
	 * Copy constructor for queries
	 * @param f
	 */
	public Farm(Farm f){
		this.id = f.getId();
		this.name = f.getName();
		this.area = f.getArea();
		this.ownerList = new ArrayList<>();
		this.tillageList = new ArrayList<>();
		this.polygon = new ArrayList<>();
		
		for(Owner o : f.getOwnerList()){
			this.ownerList.add(new Owner(o.getId(), o.getName()));
		}
		
		for(Tillage t : f.getTillageList()){
			this.tillageList.add(new Tillage(t));
		}
		
		for(Polygon p : f.getPolygon()){
			this.polygon.add(p);
		}
	}

	/**
	 * Default constructor
	 */
	public Farm(){}
	
	@Id
	@GeneratedValue(generator = "farm_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "farm_id_seq", sequenceName = "farm_id_seq", allocationSize = 1)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "area")
	private Double area;
	
	@OneToMany(mappedBy = "farm", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JsonManagedReference
	private List<Polygon> polygon;

	 @ManyToMany(fetch = FetchType.EAGER)
	 @JoinTable(name = "farm_owner", joinColumns = { @JoinColumn(name = "id_farm") }, 
	 								 inverseJoinColumns = {@JoinColumn(name = "id_owner") })
	 private List<Owner> ownerList;
	 
	 @OneToMany(mappedBy = "farm", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	 @Fetch (FetchMode.SELECT)
	 @JsonManagedReference
	 private List<Tillage> tillageList;
	 

	// Getters and Setters
	
	
	public List<Owner> getOwnerList() {
		return ownerList;
	}

	public void setOwnerList(List<Owner> owners) {
		this.ownerList = owners;
	}

	public List<Polygon> getPolygon() {
		return polygon;
	}

	public void setPolygon(List<Polygon> polygon) {
		this.polygon = polygon;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Tillage> getTillageList() {
		return tillageList;
	}

	public void setTillageList(List<Tillage> tillageList) {
		this.tillageList = tillageList;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Farm other = (Farm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
