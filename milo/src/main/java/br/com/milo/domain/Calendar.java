package br.com.milo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.milo.util.JsonCalendarSerializer;

@Entity
@Table(name = "calendar")
public class Calendar implements Serializable {

	private static final long serialVersionUID = 7064809078222302493L;

	public Calendar(){}
	
	public Calendar(Calendar c){
		this.id = c.getId();
		this.calendarDate = c.getCalendarDate();
		this.text = c.getText();
		this.ownerList = new ArrayList<>();
		
		for(Owner o : c.getOwnerList()){
			this.ownerList.add(new Owner(o.getId(), o.getName()));
		}
	}
	
	@Id
	@GeneratedValue(generator = "calendar_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "calendar_id_seq", sequenceName = "calendar_id_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "calendar_date")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private java.util.Calendar calendarDate;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "calendar_owner", 
			   joinColumns = { @JoinColumn(name = "id_calendar") }, 
			   inverseJoinColumns = {@JoinColumn(name = "id_owner") })
	private List<Owner> ownerList;
	
	public List<Owner> getOwnerList() {
		return ownerList;
	}

	public void setOwnerList(List<Owner> ownerList) {
		this.ownerList = ownerList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@JsonSerialize(using=JsonCalendarSerializer.class)
	public java.util.Calendar getCalendarDate() {
		return calendarDate;
	}

	public void setCalendarDate(java.util.Calendar calendarDate) {
		this.calendarDate = calendarDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Calendar other = (Calendar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
