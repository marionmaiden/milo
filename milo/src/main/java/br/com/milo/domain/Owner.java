package br.com.milo.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.milo.enumeration.UserType;

@Entity
@Table(name = "owner")
public class Owner implements Serializable {

	private static final long serialVersionUID = 3605331584324240290L;

	public Owner(Integer id, String name){
		this.id = id;
		this.name = name;
	}
	
	public Owner(){
	}
	
	@Id
	@GeneratedValue(generator = "owner_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "owner_id_seq", sequenceName = "owner_id_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name = "name", unique=true)
	private String name;
	
	@Column(name = "passwd")
	private String password;
	
	@Column(name = "date_sign")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSign;

	@Column(name = "email")
	private String email;
	
	@Column(name = "user_type")
	@Enumerated(EnumType.STRING)
	private UserType userType;

	public Integer getId() {
		return id;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateSign() {
		return dateSign;
	}

	public void setDateSign(Date dateSign) {
		this.dateSign = dateSign;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Owner other = (Owner) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
