
package br.com.milo.domain.DTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "arrSample"
})
public class Samples {

    @JsonProperty("arrSample")
    private List<ArrSample> arrSample = new ArrayList<ArrSample>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Samples() {
    }

    /**
     * 
     * @param arrSample
     */
    public Samples(List<ArrSample> arrSample) {
        this.arrSample = arrSample;
    }

    /**
     * 
     * @return
     *     The arrSample
     */
    @JsonProperty("arrSample")
    public List<ArrSample> getArrSample() {
        return arrSample;
    }

    /**
     * 
     * @param arrSample
     *     The arrSample
     */
    @JsonProperty("arrSample")
    public void setArrSample(List<ArrSample> arrSample) {
        this.arrSample = arrSample;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
