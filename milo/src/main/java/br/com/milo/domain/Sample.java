package br.com.milo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.milo.enumeration.FenologyType;

@Entity
@Table(name = "sample")
public class Sample implements Serializable {
	
	private static final long serialVersionUID = 7064809078222302493L;

	public Sample(){
		this.sampleItemList = new ArrayList<>();
	}
	
	@Id
	@GeneratedValue(generator = "sample_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sample_id_seq", sequenceName = "sample_id_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name = "responsible")
	private String responsible;
	
	@Column(name = "temperature")
	private Double temperature;
	
	@Column(name = "rain")
	private Integer rain;
	
	@Column(name = "fenology_stage")
	@Enumerated(EnumType.STRING)
	private FenologyType fenologyStage;

	@Column(name = "date_sample")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateSample;

	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "tillage_sample", 
	   joinColumns = { @JoinColumn(name = "id_sample") }, 
	   inverseJoinColumns = {@JoinColumn(name = "id_tillage") })
	@JsonBackReference
	private Tillage tillage = new Tillage();
	
	@OneToMany(mappedBy = "sample", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JsonManagedReference
	private List<SampleItem> sampleItemList;
	

	// Getters and Setters	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Integer getRain() {
		return rain;
	}

	public void setRain(Integer rain) {
		this.rain = rain;
	}

	public Tillage getTillage() {
		return tillage;
	}

	public void setTillage(Tillage tillage) {
		this.tillage = tillage;
	}

	public FenologyType getFenologyStage() {
		return fenologyStage;
	}

	public void setFenologyStage(FenologyType fenologyStage) {
		this.fenologyStage = fenologyStage;
	}

	public Date getDateSample() {
		return dateSample;
	}

	public void setDateSample(Date dateSample) {
		this.dateSample = dateSample;
	}

	public List<SampleItem> getSampleItemList() {
		return sampleItemList;
	}

	public void setSampleItemList(List<SampleItem> sampleItemList) {
		this.sampleItemList = sampleItemList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sample other = (Sample) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
