
package br.com.milo.domain.DTO;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "nameTillage",
    "dateInit",
    "type",
    "spacing",
    "samples",
    "usr",
    "pss"
})
public class TillageDTO {

    @JsonProperty("nameTillage")
    private String nameTillage;
    @JsonProperty("dateInit")
    private String dateInit;
    @JsonProperty("type")
    private String type;
    @JsonProperty("spacing")
    private String spacing;
    @JsonProperty("samples")
    private Samples samples;
    @JsonProperty("usr")
    private String usr;
    @JsonProperty("pss")
    private String pass;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public TillageDTO() {
    }

    /**
     * 
     * @param dateInit
     * @param nameTillage
     * @param spacing
     * @param type
     * @param samples
     */
    public TillageDTO(String nameTillage, String dateInit, String type, String spacing, Samples samples, String usr, String pass) {
        this.nameTillage = nameTillage;
        this.dateInit = dateInit;
        this.type = type;
        this.spacing = spacing;
        this.samples = samples;
        this.usr = usr;
        this.pass = pass;
    }

    /**
     * 
     * @return
     *     The nameTillage
     */
    @JsonProperty("nameTillage")
    public String getNameTillage() {
        return nameTillage;
    }

    /**
     * 
     * @param nameTillage
     *     The nameTillage
     */
    @JsonProperty("nameTillage")
    public void setNameTillage(String nameTillage) {
        this.nameTillage = nameTillage;
    }

    /**
     * 
     * @return
     *     The dateInit
     */
    @JsonProperty("dateInit")
    public String getDateInit() {
        return dateInit;
    }

    /**
     * 
     * @param dateInit
     *     The dateInit
     */
    @JsonProperty("dateInit")
    public void setDateInit(String dateInit) {
        this.dateInit = dateInit;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The spacing
     */
    @JsonProperty("spacing")
    public String getSpacing() {
        return spacing;
    }

    /**
     * 
     * @param spacing
     *     The spacing
     */
    @JsonProperty("spacing")
    public void setSpacing(String spacing) {
        this.spacing = spacing;
    }

    /**
     * 
     * @return
     *     The samples
     */
    @JsonProperty("samples")
    public Samples getSamples() {
        return samples;
    }

    /**
     * 
     * @param samples
     *     The samples
     */
    @JsonProperty("samples")
    public void setSamples(Samples samples) {
        this.samples = samples;
    }

    @JsonProperty("usr")
    public String getUsr() {
		return usr;
	}

    @JsonProperty("usr")
	public void setUsr(String usr) {
		this.usr = usr;
	}

    @JsonProperty("pss")
	public String getPass() {
		return pass;
	}

    @JsonProperty("pss")
	public void setPass(String pass) {
		this.pass = pass;
	}

	@JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
