
package br.com.milo.domain.DTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "array"
})
public class Areas {

    @JsonProperty("array")
    private List<Array> array = new ArrayList<Array>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Areas() {
    }

    /**
     * 
     * @param array
     */
    public Areas(List<Array> array) {
        this.array = array;
    }

    /**
     * 
     * @return
     *     The array
     */
    @JsonProperty("array")
    public List<Array> getArray() {
        return array;
    }

    /**
     * 
     * @param array
     *     The array
     */
    @JsonProperty("array")
    public void setArray(List<Array> array) {
        this.array = array;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
