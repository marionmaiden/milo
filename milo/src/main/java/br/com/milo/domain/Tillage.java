package br.com.milo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.milo.enumeration.TillageType;

@Entity
@Table(name = "tillage")
public class Tillage implements Serializable {

	private static final long serialVersionUID = 3605331584324240290L;

	// Default constructor
	public Tillage() {
		this.ownerList = new LinkedHashSet<>();
		this.sampleList = new ArrayList<>();
	}
	
	public Tillage(Integer id){
		this();
		this.id = id;
	}
	
	public Tillage(Tillage t){
		this.setId(t.getId());
		this.setDateInit(t.getDateInit());
		this.setName(t.getName());
		this.setSpacing(t.getSpacing());
		this.setType(t.getType());
		this.ownerList = new LinkedHashSet<>();
		this.sampleList = new ArrayList<>();
		
		for(Owner o : t.getOwnerList()){
			this.ownerList.add(new Owner(o.getId(), o.getName()));
		}
		
		for(Sample s: t.getSampleList()){
			this.sampleList.add(s);
		}
	}
	
	@Id
	@GeneratedValue(generator = "tillage_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "tillage_id_seq", sequenceName = "tillage_id_seq", allocationSize = 1)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "date_init")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateInit;

	@Column(name = "til_type")
	@Enumerated(EnumType.STRING)
	private TillageType type;

	@Column(name = "spacing")
	private Integer spacing;

	@ManyToOne
    @JoinColumn(name = "id_farm")
	@JsonBackReference
	private Farm farm;
	
	@OneToMany(mappedBy= "tillage",cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonManagedReference
	private List<Sample> sampleList;
	
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tillage_owner", 
			   joinColumns = { @JoinColumn(name = "id_tillage") }, 
			   inverseJoinColumns = {@JoinColumn(name = "id_owner") })
	private Set<Owner> ownerList;


	// Getters and Setters
	
	
	public Set<Owner> getOwnerList() {
		return ownerList;
	}

	public void setOwnerList(Set<Owner> ownerList) {
		this.ownerList = ownerList;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateInit() {
		return dateInit;
	}

	public void setDateInit(Date dateInit) {
		this.dateInit = dateInit;
	}

	public TillageType getType() {
		return type;
	}

	public void setType(TillageType type) {
		this.type = type;
	}

	public Integer getSpacing() {
		return spacing;
	}

	public void setSpacing(Integer spacing) {
		this.spacing = spacing;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Sample> getSampleList() {
		return sampleList;
	}

	public void setSampleList(List<Sample> sampleList) {
		this.sampleList = sampleList;
	}

	
	public Farm getFarm() {
		return farm;
	}

	public void setFarm(Farm farm) {
		this.farm = farm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tillage other = (Tillage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
