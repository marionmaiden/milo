package br.com.milo.domain;

import br.com.milo.enumeration.MessageType;

public class Message {

	String text;
	MessageType messageType;
	String style;
	
	public Message(){
		this.text = "";
		this.messageType = MessageType.INFO;
		this.style = this.messageType.value();
	}
	
	public Message(String text, MessageType messageType){
		this.text = text;
		this.messageType = messageType;
		this.style = this.messageType.value();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
		this.style = this.messageType.value();
	}

	public String getStyle() {
		return style;
	}
	
	
}
