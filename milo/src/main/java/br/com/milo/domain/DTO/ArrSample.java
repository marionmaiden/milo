
package br.com.milo.domain.DTO;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "date",
    "responsible",
    "fenStatus",
    "temperature",
    "rain",
    "areas"
})
public class ArrSample {

    @JsonProperty("date")
    private String date;
    @JsonProperty("responsible")
    private String responsible;
    @JsonProperty("fenStatus")
    private String fenStatus;
    @JsonProperty("temperature")
    private String temperature;
    @JsonProperty("rain")
    private String rain;
    @JsonProperty("areas")
    private Areas areas;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ArrSample() {
    }

    /**
     * 
     * @param responsible
     * @param areas
     * @param date
     * @param temperature
     * @param fenStatus
     */
    public ArrSample(String date, String responsible, String fenStatus, String temperature, String rain, Areas areas) {
        this.date = date;
        this.responsible = responsible;
        this.fenStatus = fenStatus;
        this.temperature = temperature;
        this.rain = rain;
        this.areas = areas;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The responsible
     */
    @JsonProperty("responsible")
    public String getResponsible() {
        return responsible;
    }

    /**
     * 
     * @param responsible
     *     The responsible
     */
    @JsonProperty("responsible")
    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    /**
     * 
     * @return
     *     The fenStatus
     */
    @JsonProperty("fenStatus")
    public String getFenStatus() {
        return fenStatus;
    }

    /**
     * 
     * @param fenStatus
     *     The fenStatus
     */
    @JsonProperty("fenStatus")
    public void setFenStatus(String fenStatus) {
        this.fenStatus = fenStatus;
    }

    /**
     * 
     * @return
     *     The temperature
     */
    @JsonProperty("temperature")
    public String getTemperature() {
        return temperature;
    }

    /**
     * 
     * @param temperature
     *     The temperature
     */
    @JsonProperty("temperature")
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("rain")
    public String getRain() {
		return rain;
	}

    @JsonProperty("rain")
	public void setRain(String rain) {
		this.rain = rain;
	}

	/**
     * 
     * @return
     *     The areas
     */
    @JsonProperty("areas")
    public Areas getAreas() {
        return areas;
    }

    /**
     * 
     * @param areas
     *     The areas
     */
    @JsonProperty("areas")
    public void setAreas(Areas areas) {
        this.areas = areas;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
