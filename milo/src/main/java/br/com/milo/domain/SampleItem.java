package br.com.milo.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "sample_item")
public class SampleItem implements Serializable {

	private static final long serialVersionUID = 7064809078222302493L;

	@Id
	@GeneratedValue(generator = "sample_item_id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "sample_item_id_seq", sequenceName = "sample_item_id_seq", allocationSize = 1)
	private Integer id;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "sample_sample_item", 
	   joinColumns = { @JoinColumn(name = "id_sample_item") }, 
	   inverseJoinColumns = {@JoinColumn(name = "id_sample") })
	@JsonBackReference
	private Sample sample = new Sample();

	@Column(name = "plant")
	private Integer plant;
	
	@Column(name = "weed")
	private Boolean weed;
	
	@Column(name = "plague")
	private Integer plague;
	
	@Column(name = "disease")
	private Boolean disease;
	
//	@Column(name = "rain")
//	private Integer rain;
	
	@Column(name = "production")
	private Double production;
	
	@Column(name = "lat")
	private Double lat;
	
	@Column(name = "longi")
	private Double longi;
	
	public Double getLat() {
		return lat;
	}

	
	// Getters and Setters
	
	
	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLongi() {
		return longi;
	}

	public void setLongi(Double longi) {
		this.longi = longi;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Sample getSample() {
		return sample;
	}

	public void setSample(Sample sample) {
		this.sample = sample;
	}

	public Integer getPlant() {
		return plant;
	}

	public void setPlant(Integer plant) {
		this.plant = plant;
	}

	public Boolean getWeed() {
		return weed;
	}

	public void setWeed(Boolean weed) {
		this.weed = weed;
	}

	public Integer getPlague() {
		return plague;
	}

	public void setPlague(Integer plague) {
		this.plague = plague;
	}

	public Boolean getDisease() {
		return disease;
	}

	public void setDisease(Boolean disease) {
		this.disease = disease;
	}

//	public Integer getRain() {
//		return rain;
//	}
//
//	public void setRain(Integer rain) {
//		this.rain = rain;
//	}

	public Double getProduction() {
		return production;
	}

	public void setProduction(Double production) {
		this.production = production;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SampleItem other = (SampleItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
