package br.com.milo.service;

import java.util.List;

import br.com.milo.domain.Calendar;
import br.com.milo.domain.Message;

public interface CalendarService {
	
	public List<Calendar> listByMonth(Calendar cal);
	public Message save(Calendar cal);
	public Message delete(Calendar cal);
}
