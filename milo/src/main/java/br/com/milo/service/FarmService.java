package br.com.milo.service;

import java.util.List;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;

public interface FarmService {
	
	public Message save(Farm f);
	public List<Farm> list(Owner o);
	public List<Farm> listByTillage(Owner o);
	public Message delete(Integer id);
	
}
