package br.com.milo.service;

import java.util.List;

import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.domain.Tillage;
import br.com.milo.domain.DTO.TillageDTO;

public interface TillageService {
	
	public Tillage toTillage(TillageDTO dto);
	public List<Tillage> list(Owner o);
	public Message save(Tillage tillage);
	public Message delete(Integer id);
	public Message update(Tillage tillage);
}
