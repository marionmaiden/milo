package br.com.milo.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.domain.Sample;
import br.com.milo.domain.SampleItem;
import br.com.milo.domain.Tillage;
import br.com.milo.domain.DTO.ArrSample;
import br.com.milo.domain.DTO.Array;
import br.com.milo.domain.DTO.TillageDTO;
import br.com.milo.enumeration.FenologyType;
import br.com.milo.enumeration.MessageType;
import br.com.milo.enumeration.TillageType;
import br.com.milo.enumeration.UserType;
import br.com.milo.repository.FarmRepository;
import br.com.milo.repository.OwnerRepository;
import br.com.milo.repository.SampleRepository;
import br.com.milo.repository.TillageRepository;

@Service
public class TillageServiceImpl implements TillageService {
	
	@Autowired
	OwnerRepository ownerRepository;
	
	@Autowired
	TillageRepository tillageRepository;
	
	@Autowired
	SampleRepository sampleRepository;
	
	@Autowired
	FarmRepository farmRepository;
	
	/**
	 * Convert a TillageDTO object to Tillage
	 * The TillageDTO represents the data from mobile app
	 */
	@Override
	public Tillage toTillage(TillageDTO dto) {

		// Instantiate a new tillage
		Tillage tillage = new Tillage();
		
		// Set the date format used on TillageDTO
		DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		// Try to find the designed user and then add to Tillage
		Owner o = ownerRepository.findByName(dto.getUsr());
		if(o != null){ tillage.getOwnerList().add(o);}
		else{
			return null;
		}
		
		if(dto.getNameTillage().isEmpty() || 
				dto.getDateInit().isEmpty() || 
				dto.getSpacing().isEmpty() ||
				dto.getType().isEmpty()){
			return null;
		}
		else{
			// Convert date
			try {
				tillage.setDateInit(inputDateFormat.parse(dto.getDateInit()));
			} catch (ParseException e) {
				tillage.setDateInit(new Date());
				e.printStackTrace();
			}
			// Set other attributes to tillage
			tillage.setName(dto.getNameTillage());
			tillage.setSpacing(new Integer(dto.getSpacing()));
			tillage.setType(TillageType.fromString(dto.getType()));
		}
		
		// Fill the samples of the tillage
		for(ArrSample sample : dto.getSamples().getArrSample()){
			
			Sample s = new Sample();
			
			// Convert sample date
			try{	
				s.setDateSample(inputDateFormat.parse(sample.getDate()));
			} catch (ParseException e) {
				s.setDateSample(new Date());
				e.printStackTrace();
			}
			
			// Set other attributes to sample
			s.setFenologyStage(FenologyType.valueOf(sample.getFenStatus()));
			s.setResponsible(sample.getResponsible());
			s.setTemperature(Double.parseDouble(sample.getTemperature()));
			s.setRain(Integer.parseInt(sample.getRain()));
			s.setTillage(tillage);
			
			// Fill the items of the sample
			for(Array arr : sample.getAreas().getArray()){
				
				SampleItem si = new SampleItem();
				
				// Save sampleItem coordinates
				String gps = arr.getGps();
				if(gps != null && !gps.isEmpty()){
					si.setLat(Double.parseDouble(gps.substring(0, gps.indexOf("_"))));
					si.setLongi(Double.parseDouble(gps.substring(gps.indexOf("_") + 1)));
				}
				
				// Set other attributes to sampleItem
				si.setDisease(arr.isDisease());
				si.setPlague(Integer.parseInt(arr.getPlague()));
				si.setPlant(Integer.parseInt(arr.getPlants()));
				si.setProduction(Double.parseDouble(arr.getProduction()));
				//si.setRain(Integer.parseInt(arr.getRain()));
				si.setWeed(arr.isHerbs());
				si.setSample(s);
				
				s.getSampleItemList().add(si);
			}
			
			tillage.getSampleList().add(s);
		}
		return tillage;
	}
	
	
	/**
	 * Saves a tillage
	 * @param tillage
	 * @return
	 */
	@Transactional
	public Message save(Tillage tillage){
		if(tillage.getName().isEmpty()){
			return new Message("O nome do cultivar não foi definido", MessageType.ERROR);
		}
		else if(tillage.getOwnerList().size() == 0){
			return new Message("Não foi possível localizar o proprietário", MessageType.ERROR);
		}
		else if(tillage.getSpacing() <= 0){
			return new Message("Não foi possível validar o espaçamento do cultivar", MessageType.ERROR);
		}
		else{
			tillageRepository.save(tillage);
			
			return new Message("Cultivar salvo com sucesso!", MessageType.SUCCESS);
		}
	}
	
	
	@Transactional
	@Override
	public Message update(Tillage tillage){
		
		if(tillage.getFarm() == null || tillage.getFarm().getId() == null){
			return new Message("Não foi possível localizar a propriedade", MessageType.ERROR);
		}
		else if(tillage.getId() == null || tillage.getId() <= 0 ){
			return new Message("Não foi possível localizar o cultivar", MessageType.ERROR);
		}
		else{
			Tillage t = tillageRepository.findById(tillage.getId());
			Farm f = farmRepository.findById(tillage.getFarm().getId());
			
			// If some Owner was inserted
			if(!tillage.getOwnerList().isEmpty()){
				for(Owner o : tillage.getOwnerList()){
					o = ownerRepository.findByName(o.getName());
					
					if(o != null)
						t.getOwnerList().add(o);
				}
			}
			
			if(f != null && t != null){
				t.setFarm(f);
				tillageRepository.update(t);
			}
			else{
				return new Message("Erro ao obter dados do banco.", MessageType.ERROR);
			}
			
			return new Message("Associação salva com sucesso!", MessageType.SUCCESS);
		}
		
	}
	
	
	/**
	 * Lists tillages by Owner
	 */
	@Override
	public List<Tillage> list(Owner o) {
		// Checks if the Owner exists
		if(o.getId() != null && o.getId() > 0){
			// If user isn't admin, return only tillages owned by him
			if(((Owner)ownerRepository.findById(o.getId())).getUserType() != UserType.ADMIN){
				return tillageRepository.listByType(o);
			}
			// If the user is admin, return all tillages
			else{
				return tillageRepository.list();
			}
		}
		return null;
	}
	
	
	/**
	 * Deletes a tillage
	 */
	@Override
	@Transactional
	public Message delete(Integer id) {
		Tillage t;
		// Checks if a farm with specific Id exists
		if((t = tillageRepository.findById(id)) == null){
			return new Message("O cultivar não foi localizado", MessageType.ERROR);
		}
		else{
			tillageRepository.delete(t);
			return new Message("Cultivar excluído com sucesso", MessageType.SUCCESS);
		}
	}

}
