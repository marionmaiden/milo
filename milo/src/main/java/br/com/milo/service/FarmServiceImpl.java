package br.com.milo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.domain.Polygon;
import br.com.milo.enumeration.MessageType;
import br.com.milo.enumeration.UserType;
import br.com.milo.repository.FarmRepository;
import br.com.milo.repository.OwnerRepository;
import br.com.milo.repository.PolygonRepository;

@Service
public class FarmServiceImpl implements FarmService{

	@Autowired
	private FarmRepository farmRepository;
	
	@Autowired
	private PolygonRepository polygonRepository;
	
	@Autowired
	private OwnerRepository ownerRepository;
	
	@Override
	@Transactional
	public Message save(Farm f) {
		// Checks if the polygon has at least 3 edges
		if(f.getPolygon().size() < 3){
			return new Message("A área da propriedade deve ser definida", MessageType.ERROR);
		}
		// Checks if a farm name was set
		else if(f.getName().isEmpty()){
			return new Message("O nome da propriedade não pode estar em branco", MessageType.ERROR);
		}
		// Checks if the farm name is unique for this owner
		else if(!farmRepository.listByName(f.getName(), f.getOwnerList().get(0).getId()).isEmpty()){
			return new Message("Já existe uma propriedade cadastrada com este nome", MessageType.ERROR);
		}
		else{
			farmRepository.save(f);
			
			for(Polygon p : f.getPolygon()){
				p.setFarm(f);
				polygonRepository.save(p);
			}
			
			return new Message("Propriedade salva com sucesso", MessageType.SUCCESS);
		}
	}

	
	@Override
	public List<Farm> list(Owner o) {
		// Checks if the Owner exists
		if(o.getId() != null && o.getId() > 0){
			// If user isn't admin, return only farms owned by him
			if(((Owner)ownerRepository.findById(o.getId())).getUserType() != UserType.ADMIN){
				return farmRepository.listByType(o);
			}
			// If the user is admin, return all farms
			else{
				return farmRepository.list();
			}
		}
		return null;
	}

	@Override
	public List<Farm> listByTillage(Owner o) {
		// Checks if the Owner exists
		if(o.getId() != null && o.getId() > 0){
			List<Farm> farms;
			
			// If user isn't admin, return only farms owned by him
			if(((Owner)ownerRepository.findById(o.getId())).getUserType() != UserType.ADMIN){
				farms = farmRepository.listFarmsWithTillage(o);
			}
			// If the user is admin, return all farms with tillage
			else{
				farms = farmRepository.listFarmsWithTillage();
			}
			
			for(Farm f : farms){
				f.getPolygon();
			}
			
			return farms;
		}
		return null;
	}
	

	@Override
	@Transactional
	public Message delete(Integer id) {
		Farm f;
		// Checks if a farm with specific Id exists
		if((f = farmRepository.findById(id)) == null){
			return new Message("A propriedade não foi localizada", MessageType.ERROR);
		}
		else{
			farmRepository.delete(f);
			return new Message("Propriedade excluída com sucesso", MessageType.SUCCESS);
		}
	}

}
