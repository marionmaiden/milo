package br.com.milo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Calendar;
import br.com.milo.domain.Message;
import br.com.milo.enumeration.MessageType;
import br.com.milo.repository.CalendarRepository;
import br.com.milo.repository.OwnerRepository;

@Service
@Transactional
public class CalendarServiceImpl implements CalendarService {

	@Autowired
	private OwnerRepository ownerRepository;
	
	@Autowired
	private CalendarRepository calendarRepository;
	
	/**
	 * List the calendar events by month
	 */
	@Override
	public List<Calendar> listByMonth(Calendar cal) {
		
		if(ownerRepository.findById(cal.getOwnerList().get(0).getId()) == null){
			return null;
		}
		else if(cal.getCalendarDate().get(java.util.Calendar.MONTH) < 0 || 
				cal.getCalendarDate().get(java.util.Calendar.MONTH) > 11 || 
				cal.getCalendarDate().get(java.util.Calendar.YEAR) < 2000){
			return null;
		}
		else{
			return calendarRepository.listByMonth(cal);
		}
	}

	@Override
	public Message save(Calendar cal) {
		if(ownerRepository.findById(cal.getOwnerList().get(0).getId()) == null){
			return new Message("Erro ao localizar o usuário.", MessageType.ERROR);
		}
		else if(cal.getCalendarDate().get(java.util.Calendar.MONTH) < 0 || 
				cal.getCalendarDate().get(java.util.Calendar.MONTH) > 11 || 
				cal.getCalendarDate().get(java.util.Calendar.YEAR) < 2000){
			return new Message("Não foi possível salvar o evento. Verifique a data.", MessageType.ERROR);
		}
		else{
			calendarRepository.save(cal);
			return new Message("Evento salvo com sucesso", MessageType.SUCCESS);
		}
	}

	@Override
	public Message delete(Calendar cal) {
		if(ownerRepository.findById(cal.getOwnerList().get(0).getId()) == null){
			return new Message("Erro ao localizar o usuário.", MessageType.ERROR);
		}
		else if(cal.getId() == null || cal.getId() == -1){
			return new Message("Não foi possível localizar o evento.", MessageType.ERROR);
		}
		else{
			calendarRepository.delete(cal.getId());
			return new Message("Evento excluído com sucesso", MessageType.SUCCESS);
		}
	}
	
}
