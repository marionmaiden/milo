package br.com.milo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.service.FarmService;

@RestController
@RequestMapping("/rest/farm")
public class FarmController {
	
	private static final Logger logger = LoggerFactory.getLogger(FarmController.class);
	
	@Autowired
	private FarmService farmService;

	
//	@RequestMapping(value = RestPaths.GET_FARM, method = RequestMethod.GET)
//	public @ResponseBody Farm getFarm(@PathVariable("id") int id) {
//		logger.info("Start Get FARM. ID=" + id);
//		return farmRepository.findById(id);
//	}
	
	@RequestMapping(value = RestPaths.LIST_BY_OWNER, method = RequestMethod.POST)
	public @ResponseBody List<Farm> listByOwner(@RequestBody Owner o) {
		logger.info("Start List FARM By Owner.");
		List<Farm> farms = farmService.list(o);
		return farms;
	}
	
	@RequestMapping(value = RestPaths.LIST_BY_TILLAGE, method = RequestMethod.POST)
	public @ResponseBody List<Farm> listByTillageOwner(@RequestBody Owner o) {
		logger.info("Start List FARM By Tillage and Owner.");
		List<Farm> farms = farmService.listByTillage(o);
		return farms;
	}
	
//	@RequestMapping(value = RestPaths.UPDATE_FARM, method = RequestMethod.POST)
//	public @ResponseBody Farm updateFarm(@RequestBody Farm farm) {
//		logger.info("Start Update FARM.");
//		farmRepository.update(farm);
//		return farm;
//	}
	
	@RequestMapping(value = RestPaths.INSERT, method = RequestMethod.POST)
	public @ResponseBody Message insertFarm(@RequestBody Farm farm) {
		logger.info("Start Insert FARM.");
		Message m = farmService.save(farm);
		return m;
	}
	
	@RequestMapping(value = RestPaths.DELETE, method = RequestMethod.POST)
	public @ResponseBody Message deleteFarm(@RequestBody Integer farmId) {
		logger.info("Start delete FARM.");
		return farmService.delete(farmId);
	}

}
