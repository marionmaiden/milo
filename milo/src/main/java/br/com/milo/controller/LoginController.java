package br.com.milo.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.enumeration.MessageType;
import br.com.milo.repository.OwnerRepository;

@RestController
public class LoginController {

	@Autowired
	OwnerRepository repository;
	
	@RequestMapping("/login/fazerLogin")
	public Principal user(Principal user) {
		return user;
	}

	@RequestMapping("/login/getUser")
	public @ResponseBody Message user() {
		 User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 
		 Owner owner = repository.findByName(user.getUsername());
		 
		 return new Message(owner.getId().toString(), MessageType.INFO);
	}
	
	
}
