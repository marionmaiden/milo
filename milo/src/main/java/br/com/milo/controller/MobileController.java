package br.com.milo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.milo.domain.Message;
import br.com.milo.domain.Tillage;
import br.com.milo.domain.DTO.TillageDTO;
import br.com.milo.service.TillageService;

@RestController
@RequestMapping("/mobile")
public class MobileController {

	@Autowired
	private TillageService tillageService;
	
	private static final Logger logger = LoggerFactory.getLogger(CalendarController.class);
	
	@RequestMapping(value = "/insertTillage", method = RequestMethod.POST)
	public @ResponseBody Message insert(@RequestBody TillageDTO dto) {
		Tillage tillage = tillageService.toTillage(dto);
		
		logger.info("Start Insert Tillage.");
		Message msg = tillageService.save(tillage);

		return msg;
	}
	
	
}
