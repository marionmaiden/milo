package br.com.milo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.milo.domain.Message;
import br.com.milo.domain.Owner;
import br.com.milo.domain.Tillage;
import br.com.milo.service.TillageService;

@RestController
@RequestMapping("/rest/tillage")
public class TillageController {
	
	private static final Logger logger = LoggerFactory.getLogger(TillageController.class);
	
	@Autowired
	private TillageService tillageService;

	
//	@RequestMapping(value = RestPaths.GET_FARM, method = RequestMethod.GET)
//	public @ResponseBody Farm getFarm(@PathVariable("id") int id) {
//		logger.info("Start Get FARM. ID=" + id);
//		return farmRepository.findById(id);
//	}
	
	@RequestMapping(value = RestPaths.LIST_BY_OWNER, method = RequestMethod.POST)
	public @ResponseBody List<Tillage> listByOwner(@RequestBody Owner o) {
		logger.info("Start List TILLAGE By Owner.");
		List<Tillage> tillages = tillageService.list(o);
		return tillages;
	}
	
	@RequestMapping(value = RestPaths.UPDATE, method = RequestMethod.POST)
	public @ResponseBody Message update(@RequestBody Tillage tillage) {
		logger.info("Start Update Tillage.");
		return tillageService.update(tillage);
	}
	
//	@RequestMapping(value = RestPaths.INSERT_FARM, method = RequestMethod.POST)
//	public @ResponseBody Message insertFarm(@RequestBody Farm farm) {
//		logger.info("Start Insert FARM.");
//		Message m = tillageService.save(farm);
//		return m;
//	}
	
	@RequestMapping(value = RestPaths.DELETE, method = RequestMethod.POST)
	public @ResponseBody Message delete(@RequestBody Integer tillageId) {
		logger.info("Start delete TILLAGE.");
		return tillageService.delete(tillageId);
	}

}
