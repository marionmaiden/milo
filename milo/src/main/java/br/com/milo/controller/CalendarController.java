package br.com.milo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.milo.domain.Calendar;
import br.com.milo.domain.Message;
import br.com.milo.service.CalendarService;

@RestController
@RequestMapping("/rest/calendar")
public class CalendarController {
	
	private static final Logger logger = LoggerFactory.getLogger(CalendarController.class);
	
	@Autowired
	private CalendarService service;
	
//	@RequestMapping(value = RestPaths.GET_CALENDAR, method = RequestMethod.GET)
//	public @ResponseBody Calendar getCalendar(@PathVariable("id") int id) {
//		logger.info("Start Get Calendar. ID=" + id);
//		return repository.findById(id);
//	}
	
//	@RequestMapping(value = RestPaths.LIST_CALENDARS, method = RequestMethod.GET)
//	public @ResponseBody List<Calendar> listCalendars() {
//		logger.info("Start List Calendars.");
//		List<Calendar> calendars = service.list();
//		return calendars;
//	}
	
//	@RequestMapping(value = RestPaths.LIST_CALENDAR_BY_OWNER, method = RequestMethod.GET)
//	public @ResponseBody List<Calendar> listByOwner(@RequestBody Owner o) {
//		logger.info("Start List Calendars By Owner.");
//		List<Calendar> calendars = repository.listByType(o);
//		return calendars;
//	}
//	
//	@RequestMapping(value = RestPaths.UPDATE_CALENDAR, method = RequestMethod.POST)
//	public @ResponseBody Calendar updateCalendar(@RequestBody Calendar calendar) {
//		logger.info("Start Update Calendar.");
//		repository.update(calendar);
//		return calendar;
//	}
	
	@RequestMapping(value = RestPaths.INSERT, method = RequestMethod.POST)
	public @ResponseBody Message insertCalendar(@RequestBody Calendar calendar) {
		logger.info("Start Insert Calendar.");
		Message msg = service.save(calendar);
		return msg;
	}
	
	@RequestMapping(value = RestPaths.DELETE, method = RequestMethod.POST)
	public @ResponseBody Message deleteCalendar(@RequestBody Calendar calendar) {
		logger.info("Start delete Calendar.");
		Message msg = service.delete(calendar);
		return msg;
	}

	@RequestMapping(value = RestPaths.LIST_BY_MONTH, method = RequestMethod.POST)
	public @ResponseBody List<Calendar> listByMonth(@RequestBody Calendar calendar) {
		logger.info("Start List Calendar By Month.");
		List<Calendar> calendars = service.listByMonth(calendar);
		return calendars;
	}
	
}
