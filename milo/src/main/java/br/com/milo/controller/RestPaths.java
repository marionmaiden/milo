package br.com.milo.controller;

public class RestPaths {

	public static final String GET = "/get";
	
	public static final String LIST = "/list";
	public static final String LIST_BY_MONTH = "/list_by_month";
	public static final String LIST_BY_TILLAGE = "/list_by_tillage";
	public static final String LIST_BY_OWNER = "/list_by_owner";
	
	public static final String INSERT = "/insert";
	public static final String UPDATE = "/update";
	
	public static final String DELETE = "/delete";
	
	
	
}
