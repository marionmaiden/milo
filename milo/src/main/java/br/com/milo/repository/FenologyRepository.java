package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Fenology;
import br.com.milo.enumeration.FenologyType;

@Repository
@Transactional
public class FenologyRepository implements br.com.milo.repository.Repository<Fenology, FenologyType>{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Fenology fenology) {
		entityManager.persist(fenology);
	}

	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(Fenology fenology){
		entityManager.remove(fenology);
	}
	
	@Override
	public void update(Fenology fenology){
		entityManager.merge(fenology);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Fenology> listByType(FenologyType type) {
		Query query = entityManager.createQuery("from Fenology f where f.fenologyType=:fenologyType");
		query.setParameter("fenologyType", type);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Fenology> list() {
		Query query = entityManager.createQuery("from Fenology f");
		return query.getResultList();
	}

	@Override
	public Fenology findById(Integer id) {
		Fenology fenology = entityManager.find(Fenology.class, id);
		return fenology;
	}
}
