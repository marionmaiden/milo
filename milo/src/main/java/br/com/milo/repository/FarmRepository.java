package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Owner;

@Repository
@Transactional
public class FarmRepository implements br.com.milo.repository.Repository<Farm, Owner>{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Farm farm) {
		entityManager.persist(farm);
	}

	@Override
	public void update(Farm farm) {
		entityManager.merge(farm);
	}
	
	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(Farm farm){
		entityManager.remove(farm);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Farm> list() {
		Query query = entityManager.createQuery("from Farm f order by f.id");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Farm> listByType(Owner o) {
		if(o != null && o.getId() != null){
			Query query = entityManager.createQuery(
				"select new Farm(f) from Farm f join f.ownerList o where o.id = :owner");
			query.setParameter("owner", o.getId());
			return query.getResultList();
		}
		return null;
	}

	/**
	 * Return farms that tillageList is not empty, according to the owner
	 * @param o
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Farm> listFarmsWithTillage(Owner o) {
		if(o != null && o.getId() != null){
			Query query = entityManager.createQuery(
				"select new Farm(f) from Farm f join f.ownerList o "
				+ "where o.id = :owner AND f.tillageList IS NOT EMPTY");
			query.setParameter("owner", o.getId());
			return query.getResultList();
		}
		return null;
	}

	/**
	 * Return farms that tillageList is not empty
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Farm> listFarmsWithTillage() {
		Query query = entityManager.createQuery(
				"from Farm f WHERE f.tillageList IS NOT EMPTY order by f.id");
		return query.getResultList();
	}
	
	/**
	 * Search for a farm with a specific id
	 * @param id
	 */
	@Override
	public Farm findById(Integer id) {
		Farm farm = entityManager.find(Farm.class, id);
		return farm;
	}

	/**
	 * Checks if exists a farm for a specific Owner with a given name
	 * @param name
	 * @param ownerID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Farm> listByName(String name, Integer ownerID) {
		Query query = entityManager.createQuery("from Farm f join f.ownerList o where o.id = :owner and f.name = :name");
		query.setParameter("owner", ownerID);
		query.setParameter("name", name);
		return query.getResultList();
	}
}
