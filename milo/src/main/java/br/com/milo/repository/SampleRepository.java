package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Sample;
import br.com.milo.domain.Tillage;

@Repository
@Transactional
public class SampleRepository implements br.com.milo.repository.Repository<Sample, Tillage>{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Sample sample) {
		entityManager.persist(sample);
	}

	@Override
	public void update(Sample sample) {
		entityManager.merge(sample);
	}

	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
		
	}
	
	@Override
	public void delete(Sample sample) {
		entityManager.remove(sample);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Sample> listByType(Tillage tillage) {
		Query query = entityManager.createQuery("from Sample s where s.tillage=:tillage");
		query.setParameter("tillage", tillage);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Sample> list() {
		Query query = entityManager.createQuery("from Sample s");
		return query.getResultList();
	}

	@Override
	public Sample findById(Integer id) {
		Sample sample = entityManager.find(Sample.class, id);
		return sample;
	}

}