package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Owner;
import br.com.milo.domain.Tillage;

@Repository
@Transactional
public class OwnerRepository implements br.com.milo.repository.Repository<Owner, Tillage>{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Owner owner) {
		entityManager.persist(owner);
	}

	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}

	@Override
	public void delete(Owner owner) {
		entityManager.remove(owner);
	}
	
	@Override
	public void update(Owner owner) {
		entityManager.merge(owner);
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Owner> list() {
		Query query = entityManager.createQuery("from Owner o");
		return query.getResultList();		
	}

	/**
	 * List owners from a tillage
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Owner> listByType(Tillage tillage) {
		Query query = entityManager.createQuery("from Owner o join o.tillageList t where t=:tillage");
		query.setParameter("tillage", tillage);
		return query.getResultList();
	}

	/**
	 * Find owner by id
	 */
	@Override
	public Owner findById(Integer id) {
		Owner owner = entityManager.find(Owner.class, id);
		return owner;
	}
	
	
	@SuppressWarnings("unchecked")
	public Owner findByName(String name) {
		Query query = entityManager.createQuery("from Owner o where o.name=:name");
		query.setParameter("name", name);
		List<Owner> list = (List<Owner>) query.getResultList();
		
		return list.size() > 0 ? list.get(0) : null;
	}
}
