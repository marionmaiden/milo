package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Calendar;
import br.com.milo.domain.Owner;

@Repository
@Transactional
public class CalendarRepository implements br.com.milo.repository.Repository<Calendar, Owner>{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Calendar calendar) {
		entityManager.persist(calendar);
	}

	@Override
	public void update(Calendar calendar) {
		entityManager.merge(calendar);
	}
	
	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(Calendar calendar){
		entityManager.remove(calendar);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Calendar> list() {
		Query query = entityManager.createQuery("from Calendar c order by c.calendarDate");
		return query.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Calendar> listByType(Owner o) {
		if(o != null){
			Query query = entityManager.createQuery("from Calendar c join c.ownerList o where o.name = :owner");
			query.setParameter("owner", o.getName());
			return query.getResultList();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Calendar> listByMonth(Calendar cal) {
		
		if(cal.getOwnerList().size() > 0){
			Query query = entityManager.createQuery("select new Calendar(c) from Calendar c join c.ownerList o "
				+ "where o.id = :owner "
				+ "and month(c.calendarDate)=:month "
				+ "and year(c.calendarDate)=:year "
				+ "order by c.calendarDate");
			query.setParameter("owner", cal.getOwnerList().get(0).getId());
			query.setParameter("month", cal.getCalendarDate().get(java.util.Calendar.MONTH) + 1);
			query.setParameter("year", cal.getCalendarDate().get(java.util.Calendar.YEAR));
			return query.getResultList();
		}
		return null;
	}
	
	@Override
	public Calendar findById(Integer id) {
		Calendar calendar = entityManager.find(Calendar.class, id);
		return calendar;
	}

}
