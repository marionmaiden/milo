package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Farm;
import br.com.milo.domain.Polygon;

@Repository
@Transactional
public class PolygonRepository implements br.com.milo.repository.Repository<Polygon, Farm>{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(Polygon polygon) {
		entityManager.persist(polygon);
	}

	@Override
	public void update(Polygon polygon) {
		entityManager.merge(polygon);
	}
	
	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(Polygon polygon){
		entityManager.remove(polygon);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Polygon> list() {
		Query query = entityManager.createQuery("from Polygon p order by p.id");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Polygon> listByType(Farm farm) {
		if(farm != null){
			Query query = entityManager.createQuery("from Polygon p join p.farm f where f.id = :farm");
			query.setParameter("farm", farm.getId());
			return query.getResultList();
		}
		return null;
	}

	@Override
	public Polygon findById(Integer id) {
		Polygon polygon = entityManager.find(Polygon.class, id);
		return polygon;
	}
}
