package br.com.milo.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface Repository<Element, Element2> {

	@Transactional
	public void save(Element e);
	@Transactional
	public void delete(Integer id);
	@Transactional
	public void delete(Element e);
	@Transactional
	public void update(Element e);
	public Element findById(Integer id);
	public List<Element> list();
	public List<Element> listByType(Element2 e2);
}
