package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Owner;
import br.com.milo.domain.Tillage;

@Repository
@Transactional
public class TillageRepository implements br.com.milo.repository.Repository<Tillage, Owner>{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(Tillage tillage) {
		entityManager.persist(tillage);
	}

	@Override
	public void update(Tillage tillage) {
		entityManager.merge(tillage);
	}
	
	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(Tillage tillage){
		entityManager.remove(tillage);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Tillage> list() {
		Query query = entityManager.createQuery("from Tillage t order by t.id");
		return query.getResultList();
	}

	/**
	 * Returns only those tillages that doesn't have a farm associated
	 * @param Owner
	 * @return List<Tillage>
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Tillage> listByType(Owner owner) {
		Query query = entityManager.createQuery("select new Tillage(t) from Tillage t join t.ownerList o "
				+ "where o.id=:owner and t.farm IS NULL");
		query.setParameter("owner", owner.getId());
		return query.getResultList();
	}

	@Override
	public Tillage findById(Integer id) {
		Tillage tillage = entityManager.find(Tillage.class, id);
		return tillage;
	}
}
