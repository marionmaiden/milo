package br.com.milo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.milo.domain.Sample;
import br.com.milo.domain.SampleItem;

@Repository
@Transactional
public class SampleItemRepository implements br.com.milo.repository.Repository<SampleItem, Sample>{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(SampleItem sampleItem) {
		entityManager.persist(sampleItem);
	}
	
	@Override
	public void update(SampleItem sampleItem) {
		entityManager.merge(sampleItem);
		
	}

	@Override
	public void delete(Integer id) {
		this.delete(this.findById(id));
	}
	
	@Override
	public void delete(SampleItem sampleItem) {
		entityManager.remove(sampleItem);
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<SampleItem> listByType(Sample sample) {
		Query query = entityManager.createQuery("from SampleItem si where si.sample=:sample");
		query.setParameter("sample", sample);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SampleItem> list() {
		Query query = entityManager.createQuery("from SampleItem si");
		return query.getResultList();
	}

	@Override
	public SampleItem findById(Integer id) {
		SampleItem sampleItem = entityManager.find(SampleItem.class, id);
		return sampleItem;
	}
}
