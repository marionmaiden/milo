package br.com.milo.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonCalendarSerializer extends JsonSerializer<Calendar>{

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void serialize(Calendar cal, JsonGenerator gen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		
		dateFormat.setTimeZone(cal.getTimeZone());
		String formattedDate = dateFormat.format(cal.getTime());
		gen.writeString(formattedDate);
		
	}

}
