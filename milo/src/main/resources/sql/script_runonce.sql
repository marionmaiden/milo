
CREATE SEQUENCE tillage_id_seq INCREMENT BY 1;
CREATE SEQUENCE fenology_id_seq INCREMENT BY 1;
CREATE SEQUENCE sample_id_seq INCREMENT BY 1;
CREATE SEQUENCE sample_item_id_seq INCREMENT BY 1;
CREATE SEQUENCE owner_id_seq INCREMENT BY 1;
CREATE SEQUENCE polygon_id_seq INCREMENT BY 1;

CREATE TABLE "tillage"(
id INTEGER DEFAULT nextval('tillage_id_seq') NOT NULL,
id_farm INTEGER,
name VARCHAR(60) NOT NULL,
date_init DATE NOT NULL,
til_type VARCHAR(20) NOT NULL,
spacing INTEGER NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "fenology"(
id INTEGER DEFAULT nextval('fenology_id_seq') NOT NULL,
name VARCHAR(20) NOT NULL,
til_type VARCHAR(20) NOT NULL,
fenology_type VARCHAR(20) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "sample"(
id INTEGER DEFAULT nextval('sample_id_seq') NOT NULL,
responsible VARCHAR(80) NOT NULL,
temperature DECIMAL(5,2) NOT NULL,
rain SMALLINT NOT NULL,
fenology_stage VARCHAR(20) NOT NULL,
date_sample DATE NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "sample_item"(
id INTEGER DEFAULT nextval('sample_item_id_seq') NOT NULL,
plant SMALLINT,
plague SMALLINT,
weed BOOLEAN DEFAULT FALSE,
disease BOOLEAN DEFAULT FALSE,
production DECIMAL(10,2),
lat DECIMAL(20,10),
longi DECIMAL(20,10),
PRIMARY KEY (id));

CREATE TABLE "owner"(
id INTEGER DEFAULT nextval('owner_id_seq') NOT NULL,
name VARCHAR(80) NOT NULL,
passwd VARCHAR(255),
date_sign TIMESTAMP NOT NULL,
email VARCHAR(30) NOT NULL,
user_type VARCHAR(10) NOT NULL,
PRIMARY KEY(id));

CREATE TABLE "tillage_owner"(
id_tillage INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_tillage,id_owner));

CREATE TABLE "polygon"(
id INTEGER DEFAULT nextval('polygon_id_seq') NOT NULL,
id_farm INTEGER NOT NULL,
lat DECIMAL(20,10) NOT NULL,
longi DECIMAL(20,10) NOT NULL,
PRIMARY KEY (id));

ALTER TABLE "owner" ADD CONSTRAINT owner_name_un UNIQUE (name);
ALTER TABLE "polygon" ADD CONSTRAINT polygon_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);
ALTER TABLE "tillage_owner" ADD CONSTRAINT tillage_owner_fk FOREIGN KEY (id_tillage) REFERENCES "tillage" (id);
ALTER TABLE "tillage_owner" ADD CONSTRAINT tillage_owner_2_fk FOREIGN KEY (id_owner) REFERENCES "owner" (id);

-- ------------------------------------------------------------------------------------------------------------

-- CREATING FARM TABLE
CREATE SEQUENCE farm_id_seq INCREMENT BY 1;

CREATE TABLE "farm"(
id INTEGER DEFAULT nextval('farm_id_seq') NOT NULL,
name VARCHAR(80) NOT NULL,
area DECIMAL(20,2) NOT NULL DEFAULT 0,
PRIMARY KEY (id));

ALTER TABLE "tillage" ADD CONSTRAINT tillage_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);

-- CREATE TABLE FARM_OWNER
CREATE TABLE "farm_owner"(
id_farm INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_farm,id_owner));

ALTER TABLE "farm_owner" ADD CONSTRAINT farm_owner_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);
ALTER TABLE "farm_owner" ADD CONSTRAINT farm_owner_fk2 FOREIGN KEY (id_owner) REFERENCES "owner" (id);

-- CREATE CALENDAR SEQUENCE
CREATE SEQUENCE calendar_id_seq INCREMENT BY 1;

-- CREATE TABLE CALENDAR
CREATE TABLE "calendar"(
id INTEGER DEFAULT nextval('calendar_id_seq') NOT NULL,
text VARCHAR(200) NOT NULL,
calendar_date DATE NOT NULL,
PRIMARY KEY (id));

-- CREATE TABLE CALENDAR OWNER
CREATE TABLE "calendar_owner"(
id_calendar INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_calendar,id_owner));

ALTER TABLE "calendar_owner" ADD CONSTRAINT calendar_owner_fk FOREIGN KEY (id_calendar) REFERENCES "calendar" (id);
ALTER TABLE "calendar_owner" ADD CONSTRAINT calendar_owner_fk2 FOREIGN KEY (id_owner) REFERENCES "owner" (id);


-- ------------------------------------------------------------------------------------------------------------

-- CREATING TABLES FOR BI-DIRECTIONAL RELATIONSHIP BETWEEN SAMPLE AND TILLAGE
CREATE TABLE "tillage_sample"(
id_tillage INTEGER NOT NULL,
id_sample INTEGER NOT NULL,
PRIMARY KEY(id_tillage,id_sample));

ALTER TABLE "tillage_sample" ADD CONSTRAINT tillage_sample_fk FOREIGN KEY (id_tillage) REFERENCES "tillage" (id);
ALTER TABLE "tillage_sample" ADD CONSTRAINT tillage_sample_fk2 FOREIGN KEY (id_sample) REFERENCES "sample" (id);

-- DROPPING PREVIOUS RELATIONSHIP BETWEEN SAMPLE AND TILLAGE
ALTER TABLE "sample" drop column id_tillage CASCADE;


-- CREATING TABLES FOR BI-DIRECTIONAL RELATIONSHIP BETWEEN SAMPLE AND SAMPLE_ITEM
CREATE TABLE "sample_sample_item"(
id_sample INTEGER NOT NULL,
id_sample_item INTEGER NOT NULL,
PRIMARY KEY(id_sample,id_sample_item));

ALTER TABLE "sample_sample_item" ADD CONSTRAINT sample_sample_item_fk FOREIGN KEY (id_sample) REFERENCES "sample" (id);
ALTER TABLE "sample_sample_item" ADD CONSTRAINT sample_sample_item_fk2 FOREIGN KEY (id_sample_item) REFERENCES "sample_item" (id);



