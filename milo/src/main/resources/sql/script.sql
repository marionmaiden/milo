
CREATE SEQUENCE tillage_id_seq INCREMENT BY 1;
CREATE SEQUENCE fenology_id_seq INCREMENT BY 1;
CREATE SEQUENCE sample_id_seq INCREMENT BY 1;
CREATE SEQUENCE sample_item_id_seq INCREMENT BY 1;
CREATE SEQUENCE owner_id_seq INCREMENT BY 1;
CREATE SEQUENCE polygon_id_seq INCREMENT BY 1;
CREATE SEQUENCE estate_id_seq INCREMENT BY 1;

CREATE TABLE "tillage"(
id INTEGER DEFAULT nextval('tillage_id_seq') NOT NULL,
name VARCHAR(60) NOT NULL,
date_init DATE NOT NULL,
til_type VARCHAR(20) NOT NULL,
spacing INTEGER NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "fenology"(
id INTEGER DEFAULT nextval('fenology_id_seq') NOT NULL,
name VARCHAR(20) NOT NULL,
til_type VARCHAR(20) NOT NULL,
fenology_type VARCHAR(20) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "sample"(
id INTEGER DEFAULT nextval('sample_id_seq') NOT NULL,
id_tillage INTEGER NOT NULL,
responsible VARCHAR(80) NOT NULL,
temperature DECIMAL(5,2) NOT NULL,
fenology_stage VARCHAR(20) NOT NULL,
date_sample DATE NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "sample_item"(
id INTEGER DEFAULT nextval('sample_item_id_seq') NOT NULL,
id_sample INTEGER NOT NULL,
plant SMALLINT NOT NULL,
plague SMALLINT NOT NULL,
rain SMALLINT NOT NULL,
weed BOOLEAN NOT NULL,
disease BOOLEAN NOT NULL,
production DECIMAL(10,2) NOT NULL,
lat DECIMAL(20,10) NOT NULL,
longi DECIMAL(20,10) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "owner"(
id INTEGER DEFAULT nextval('owner_id_seq') NOT NULL,
name VARCHAR(80) NOT NULL,
password VARCHAR(50) NOT NULL,
date_sign TIMESTAMP NOT NULL,
email VARCHAR(30) NOT NULL,
user_type VARCHAR(10) NOT NULL,
PRIMARY KEY(id));

CREATE TABLE "tillage_owner"(
id_tillage INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_tillage,id_owner));

CREATE TABLE "estate"(
id INTEGER DEFAULT nextval('estate_id_seq') NOT NULL,
name VARCHAR(80) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE "polygon"(
id INTEGER DEFAULT nextval('polygon_id_seq') NOT NULL,
id_estate INTEGER NOT NULL,
lat DECIMAL(20,10) NOT NULL,
longi DECIMAL(20,10) NOT NULL,
PRIMARY KEY (id));

ALTER TABLE "sample" ADD CONSTRAINT tillage_fk FOREIGN KEY (id_tillage) REFERENCES "tillage" (id);
ALTER TABLE "sample_item" ADD CONSTRAINT sample_item_fk FOREIGN KEY (id_sample) REFERENCES "sample" (id);
ALTER TABLE "polygon" ADD CONSTRAINT polygon_fk FOREIGN KEY (id_estate) REFERENCES "estate" (id);
ALTER TABLE "tillage_owner" ADD CONSTRAINT tillage_owner_fk FOREIGN KEY (id_tillage) REFERENCES "tillage" (id);
ALTER TABLE "tillage_owner" ADD CONSTRAINT tillage_owner_2_fk FOREIGN KEY (id_owner) REFERENCES "owner" (id);



-- ------------------------------------------------------------------------------------------------------------

CREATE TABLE "estate_owner"(
id_estate INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_estate,id_owner));

ALTER TABLE "estate_owner" ADD CONSTRAINT estate_owner_fk FOREIGN KEY (id_estate) REFERENCES "tillage" (id);
ALTER TABLE "estate_owner" ADD CONSTRAINT estate_owner_fk2 FOREIGN KEY (id_owner) REFERENCES "owner" (id);

CREATE TABLE "users"(
name VARCHAR(80) NOT NULL UNIQUE,
password VARCHAR(60) NOT NULL,
PRIMARY KEY (name));

ALTER TABLE users ADD CONSTRAINT users_name_un UNIQUE (name);

-- DROPPING PASSWORD FROM OWNER, AS IT USES USERS TABLE FROM NOW ON
ALTER TABLE "owner" DROP COLUMN password CASCADE;
ALTER TABLE "owner" ADD CONSTRAINT owner_users_fk FOREIGN KEY (name) REFERENCES "users" (name);

-- ------------------------------------------------------------------------------------------------------------

-- DROPPING ESTATE TABLE
ALTER TABLE "polygon" DROP CONSTRAINT polygon_fk;
DROP TABLE "estate";
DROP SEQUENCE estate_id_seq;

-- CREATING FARM TABLE
CREATE SEQUENCE farm_id_seq INCREMENT BY 1;

CREATE TABLE "farm"(
id INTEGER DEFAULT nextval('farm_id_seq') NOT NULL,
name VARCHAR(80) NOT NULL,
PRIMARY KEY (id));

-- ALTERING TABLE POLYGON
ALTER TABLE "polygon" RENAME id_estate TO id_farm;
ALTER TABLE "polygon" ADD CONSTRAINT polygon_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);

-- DROP TABLE ESTATE_OWNER
DROP CASCADE TABLE "estate_owner";

-- CREATE TABLE FARM_OWNER
CREATE TABLE "farm_owner"(
id_farm INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_farm,id_owner));

ALTER TABLE "farm_owner" ADD CONSTRAINT farm_owner_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);
ALTER TABLE "farm_owner" ADD CONSTRAINT farm_owner_fk2 FOREIGN KEY (id_owner) REFERENCES "owner" (id);

-- CREATE CALENDAR SEQUENCE
CREATE SEQUENCE calendar_id_seq INCREMENT BY 1;

-- CREATE TABLE CALENDAR
CREATE TABLE "calendar"(
id INTEGER DEFAULT nextval('calendar_id_seq') NOT NULL,
text VARCHAR(200) NOT NULL,
calendar_date DATE NOT NULL,
PRIMARY KEY (id));

-- CREATE TABLE CALENDAR OWNER
CREATE TABLE "calendar_owner"(
id_calendar INTEGER NOT NULL,
id_owner INTEGER NOT NULL,
PRIMARY KEY(id_calendar,id_owner));

ALTER TABLE "calendar_owner" ADD CONSTRAINT calendar_owner_fk FOREIGN KEY (id_calendar) REFERENCES "calendar" (id);
ALTER TABLE "calendar_owner" ADD CONSTRAINT calendar_owner_fk2 FOREIGN KEY (id_owner) REFERENCES "owner" (id);


-- ------------------------------------------------------------------------------------------------------------
-- DROPPING TABLE USERS AND CONSTRAINTS
DROP TABLE "users" CASCADE;

-- ALTERING TABLE OWNER
ALTER TABLE "owner" ADD COLUMN passwd VARCHAR(255);
ALTER TABLE "owner" ADD CONSTRAINT owner_name_un UNIQUE (name);


-- ADD COLUMN AREA TO FARM

ALTER TABLE "farm" ADD COLUMN area DECIMAL(20,2) NOT NULL DEFAULT 0;


-- ------------------------------------------------------------------------------------------------------------

-- CREATING TABLES FOR BI-DIRECTIONAL RELATIONSHIP BETWEEN SAMPLE AND TILLAGE
CREATE TABLE "tillage_sample"(
id_tillage INTEGER NOT NULL,
id_sample INTEGER NOT NULL,
PRIMARY KEY(id_tillage,id_sample));

ALTER TABLE "tillage_sample" ADD CONSTRAINT tillage_sample_fk FOREIGN KEY (id_tillage) REFERENCES "tillage" (id);
ALTER TABLE "tillage_sample" ADD CONSTRAINT tillage_sample_fk2 FOREIGN KEY (id_sample) REFERENCES "sample" (id);


-- DROPPING PREVIOUS RELATIONSHIP BETWEEN SAMPLE AND TILLAGE
ALTER TABLE "sample" drop column id_tillage CASCADE;


-- CREATING TABLES FOR BI-DIRECTIONAL RELATIONSHIP BETWEEN SAMPLE AND SAMPLE_ITEM
CREATE TABLE "sample_sample_item"(
id_sample INTEGER NOT NULL,
id_sample_item INTEGER NOT NULL,
PRIMARY KEY(id_sample,id_sample_item));

ALTER TABLE "sample_sample_item" ADD CONSTRAINT sample_sample_item_fk FOREIGN KEY (id_sample) REFERENCES "sample" (id);
ALTER TABLE "sample_sample_item" ADD CONSTRAINT sample_sample_item_fk2 FOREIGN KEY (id_sample_item) REFERENCES "sample_item" (id);

-- DROPPING PREVIOUS RELATIONSHIP BETWEEN SAMPLE AND SAMPLE_ITEM
ALTER TABLE "sample_item" drop column id_sample CASCADE;

-- ----------------------------------------------------------------------------------------------------------

ALTER TABLE "tillage" ADD COLUMN "id_farm" INTEGER;
ALTER TABLE "tillage" ADD CONSTRAINT tillage_fk FOREIGN KEY (id_farm) REFERENCES "farm" (id);

-- ----------------------------------------------------------------------------------------------------------

-- DROPPING NOT NULL COLUMNS FROM SAMPLE_ITEM

ALTER TABLE "sample_item" 
ALTER COLUMN "lat" DROP NOT NULL,
ALTER COLUMN "longi" DROP NOT NULL,
ALTER COLUMN "plant" DROP NOT NULL,
ALTER COLUMN "plague" DROP NOT NULL,
ALTER COLUMN "rain" DROP NOT NULL,
ALTER COLUMN "weed" DROP NOT NULL,
ALTER COLUMN "weed" SET DEFAULT FALSE,
ALTER COLUMN "disease" DROP NOT NULL,
ALTER COLUMN "disease" SET DEFAULT FALSE,
ALTER COLUMN "production" DROP NOT NULL;

-- ----------------------------------------------------------------------------------------------------------
-- CHANGING ATTRIBUTE RAIN FROM SAMPLEITEM TO SAMPLE

ALTER TABLE "sample_item" DROP COLUMN "rain";
ALTER TABLE "sample" ADD COLUMN "rain" INTEGER NOT NULL DEFAULT 0;
