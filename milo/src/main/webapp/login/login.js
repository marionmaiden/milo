angular.module('loginApp',[])
.controller('loginController', ['$http', '$scope', '$rootScope', '$location', function ($http, $scope, $rootScope, $location) {
	$scope.authenticate = function (callback) {
		var headers = $scope.j_username && $scope.j_password ? 
				{authorization : "Basic " + btoa( $scope.j_username + ":" + $scope.j_password)} : 
				{};
				
	    $http.get('fazerLogin', {headers : headers}).then(
	    	function(response) {
				if (response.data.name) {
					$rootScope.authenticated = true;
				} else {
					$rootScope.authenticated = false;
				}
				callback && callback();
	    	},
	    	function(response) {
	    		$rootScope.authenticated = false;
	    		callback && callback();
	    	}
	    );
	}
	
	
	$scope.authenticate();
	$scope.credentials = {};
	
	$scope.login = function() {
		$scope.authenticate(
			function() {
				if ($rootScope.authenticated) {
					window.location = "/milo/pages/main.html";
					$scope.error = false;
				} else {
					window.location = "/milo/login/login.html";
					$scope.error = true;
				}
			}
		);
	};
	
	
}]);
