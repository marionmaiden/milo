app.controller('AnalysisController', ['$scope', 'AnalysisService', 'MapService', 'GraphService',
                              function($scope, AnalysisService, MapService, GraphService){
	
	// Farm selected
	$scope.analyzedFarm = {};
	// Tillage selected
	$scope.analyzedTillage = null;
	// Sample selected
	$scope.analyzedSample = null;
	// Param selected for chart
	$scope.chartParam = null;
	
	// Returns map area
	$scope.area = function(){
		return MapService.getArea();
	}
	
	// Returns map polygon
	$scope.polygon = function(){
		return MapService.getPolygon();
	}
	
	// Initialize Map and draw Farm polygon
	$scope.initializeMap = function() {
		MapService.initializeMap(false, false);
		$scope.drawPolygon();
		
		console.log($scope.analyzedFarm);
	}
	
	// Draw Farm polygon 
	$scope.drawPolygon = function(){
		MapService.drawPolygon($scope.analyzedFarm.polygon);
	}
	
	// Get from service the Farm to analyze
	$scope.getFarm = function(){
		$scope.analyzedFarm = AnalysisService.get();
	}
	
	// Initialize Graph
	$scope.initializeGraph =  function(){
		
		if($scope.chartParam !== null && $scope.analyzedTillage !== null){
			var labels = [];
			var series = [];
			
			for(i = 0; i < $scope.analyzedTillage.sampleList.length; i++){
				
				labels.push("Data: " + $scope.analyzedTillage.sampleList[i].dateSample + "<br/>" + 
						    "Estádio: " + $scope.analyzedTillage.sampleList[i].fenologyStage);
				
				series.push([]);
				series.push([]);
				series.push([]);
				series.push([]);
				series.push([]);
				
				//console.log(series);
				
				// Show temperature chart
				if($scope.chartParam === 'temp'){
					series[2].push($scope.analyzedTillage.sampleList[i].temperature);
				}
				// Show rain chart
				else if($scope.chartParam === 'rain'){
					series[2].push($scope.analyzedTillage.sampleList[i].rain);
				}
				else{
					var avg = 0;
					
					for(j = 0; j < $scope.analyzedTillage.sampleList[i].sampleItemList.length; j++){
						
//						console.log($scope.analyzedTillage.sampleList[i].sampleItemList[j][$scope.chartParam]);
						
						series[j].push($scope.analyzedTillage.sampleList[i].sampleItemList[j]["" + $scope.chartParam]);
						
						
						//avg+= $scope.analyzedTillage.sampleList[i].sampleItemList[j][$scope.chartParam];
					}
				}
			}
			
			GraphService.initGraph(labels, series);
		}
		else{
			GraphService.initGraph();
		}
	}
	
	// Gather data from farm to create the heat map
	$scope.heatMapAnalisys = function(param){
		// If a sample was selected
		if($scope.analyzedSample){
			var heatMapData = [];
			var markers = [];
			var i;
			var weight;
			
			// loops through all sampleItens of analyzedSample
			for(i = 0; i < $scope.analyzedSample.sampleItemList.length; i++){
				weight = $scope.analyzedSample.sampleItemList[i][param] === true ? 10 : 
					$scope.analyzedSample.sampleItemList[i][param] === false ? 1 : $scope.analyzedSample.sampleItemList[i][param];
				
				// Create heat map object array
				heatMapData.push({
					'location' : new google.maps.LatLng($scope.analyzedSample.sampleItemList[i].lat, 
														$scope.analyzedSample.sampleItemList[i].longi),
					'weight'   : weight
				});
				
				weight = $scope.analyzedSample.sampleItemList[i][param] === true ? "presente" : 
					$scope.analyzedSample.sampleItemList[i][param] === false ? "ausente" : $scope.analyzedSample.sampleItemList[i][param];
				
				// Create markers array
				markers.push(['Área ' + (i+1), 
				              $scope.analyzedSample.sampleItemList[i].lat,
				              $scope.analyzedSample.sampleItemList[i].longi,
				              weight]);
			}
			// Call method from Service
			MapService.heatMap(heatMapData, markers);
		}
	}
	
}]);