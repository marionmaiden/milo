app.service('CalendarService',['$http', '$q', function($http,$q){
		
	this.errorMessage = {text: "Erro ao acessar o servidor",
  			 style: "alert alert-dismissible alert-warning",
  			 messageType: "ERROR"};
	
	// Lists the calendars by Owner ID
	this.listCalendar = function(date, idUser){
		var deferred = $q.defer();
		
		var parameter = {
				calendarDate: date,
				text: "",
				ownerList: [{id : idUser}]
			};
			
		parameter = JSON.stringify(parameter);
		
        $http.post('/milo/rest/calendar/list_by_month',parameter).then(
        	function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
                else {
                    deferred.reject('Error retrieving list of events');
                }
        	},
        	function(response){
        		deferred.reject('Error retrieving list of events');
        	}
        );
        return deferred.promise;
	}
	
	
	// Inserts a new Calendar in the server
	this.insertCalendar = function(date, text, idUser){
		var deferred = $q.defer();
		
		var parameter = {
				calendarDate: date,
				text: text,
				ownerList: [{id : idUser}]
			};
			
		parameter = JSON.stringify(parameter);
		
        $http.post('/milo/rest/calendar/insert',parameter).then(
    		function(response) {
				deferred.resolve(response.data);
			},
    			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
        return deferred.promise;
	}
	
	
	// Removes a new Calendar in the server
	this.removeCalendar = function(calendar, idUser){
		var deferred = $q.defer();
		
		calendar.ownerList = [{id : idUser}];;
			
		parameter = JSON.stringify(calendar);
		
        $http.post('/milo/rest/calendar/delete',parameter).then(
    		function(response) {
				deferred.resolve(response.data);
			},
    			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
        return deferred.promise;
	}
	
}]); 


