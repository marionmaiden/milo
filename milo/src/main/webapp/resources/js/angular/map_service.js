app.service('MapService',['$rootScope',function($rootScope){
		
	var pos = null;
	var map = null;
	var heatmap = null;
	var drawingManager = null;
	var selectedShape = null;
	var polygon = null;
	var area = 0;
	
	//store marker in array
	var pointMarker = [];
	
	// Show controllers for farm drawing
	var showControllers = true;
	// Set center based on geolocation
	var showCenter = true;
	
	// Parameters for the map
	var mapParams = {	
		zoom : 15,
		streetViewControl : false,
		mapTypeControl : false,
		mapTypeId : google.maps.MapTypeId.HYBRID
	};
	
	var polygonColor = {
			fillColor : '#00CCCC',
			fillOpacity : 0.2,
			strokeWeight : 2,
			zIndex: 1}
	
	// Parameters for the polygon
	var drawablePolygon = {
		clickable : true,
		editable : true,
    };
	
	/**
	 * Returns the area
	 */
	this.getArea = function(){
		return area;
	}
	
	/**
	 * Returns the polygon
	 */
	this.getPolygon = function(){
		return polygon;
	}
	
    /**
     * Initialize Google Maps
     */
	this.initializeMap = function(controllers, center) {
		showControllers = controllers;
		showCenter = center;
		
		// Initialize map on map_canvas div
		map = new google.maps.Map(document.getElementById("map_canvas"), mapParams);

		// If the browser supports geolocation
		if(showCenter){
			if (navigator.geolocation) {
				this.setGeolocation();
			}
			else {
				map.setCenter(new google.maps.LatLng(37.7749295, -122.4194155));
			}
		}
		
		// Enables drawing on Map
		if(showControllers)
			this.setDrawing();
	}
	
	
	/**
	 * Sets geolocation for the map
	 * Centers the map based on user location
	 */
	this.setGeolocation = function() {
		navigator.geolocation.getCurrentPosition(function(position) {
			pos = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};
			
			var marker = new google.maps.Marker({
			    position: pos,
			    map: map,
			    title:"Você está aqui"
			});
			// Centers the map on position
			map.setCenter(pos);
		});
	}
	
	/**
	 * Enables map to make drawings to set the property limits
	 */
	this.setDrawing = function(){
		var polygonParams = {};
		$.extend(polygonParams, polygonColor, drawablePolygon);
		
		// Initializes the drawing manager
		drawingManager = new google.maps.drawing.DrawingManager({
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_LEFT,
				drawingModes: [google.maps.drawing.OverlayType.POLYGON]
			},
			polygonOptions: polygonParams
		});
		
		// Create a custom Div for the "delete" button
		var customControlDiv = document.createElement('div');
	    var customControl = this.CustomControl(customControlDiv);
	    customControlDiv.index = 1;
	    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(customControlDiv);
		
	    // Adds a event listener for the polygon
		google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
			// Polygon draw event
			if (event.type == google.maps.drawing.OverlayType.POLYGON) {
				// Polygon vertex editing event 
				google.maps.event.addListener(event.overlay.getPath(), 'insert_at', function(index, obj) {
					polygon = event.overlay;
					$rootScope.$apply(function () { 
						area = (google.maps.geometry.spherical.computeArea(polygon.getPath()) / 1000).toFixed(2);
					});
			    });
				// Polygon edge editing event
			    google.maps.event.addListener(event.overlay.getPath(), 'set_at', function(index, obj) {
			    	polygon = event.overlay;
			    	$rootScope.$apply(function () { 
			    		area = (google.maps.geometry.spherical.computeArea(polygon.getPath()) / 1000).toFixed(2);
					});
			    });
			    polygon = event.overlay;
			    $rootScope.$apply(function () { 
			    	area = (google.maps.geometry.spherical.computeArea(polygon.getPath()) / 1000).toFixed(2);
				});
			    
				// Switch back to non-drawing mode after drawing a shape.
				drawingManager.setDrawingMode(null);
				drawingManager.setOptions({
					  drawingControl: false
				});
			 }
			});

		drawingManager.setMap(map);
	}
	
	
	/**
	 * Adds a div with "clear map" button 
	 * @param controlDiv
	 */
	this.CustomControl = function (controlDiv) {
	    // Set CSS for the control border
	    var controlUI = document.createElement('div');
	    controlUI.style.backgroundColor = 'white';
	    controlUI.style.height = '24px';
	    controlUI.style.marginTop = '6px';
	    controlUI.style.marginRight = '6px';
	    controlUI.style.cursor = 'default';
	    controlUI.style.borderRadius="2px";
	    controlUI.title = 'Limpar mapa';

	    controlUI.onmouseover = function() {
	    	this.style.backgroundColor = "Gainsboro";
	    }
	    controlUI.onmouseout = function() {
	    	this.style.backgroundColor = "white";
	    }
	    
	    controlDiv.appendChild(controlUI);

	    // Set CSS for the control interior
	    var controlText = document.createElement('div');
	    controlText.style.fontSize = "16px";
	    controlText.style.paddingLeft = '6px';
	    controlText.style.paddingRight = '6px';
	    controlText.style.paddingTop = '4px';
	    controlText.innerHTML = '<i class="fa fa-trash"></i>';
	    controlUI.appendChild(controlText);

	    // Setup the click event listeners
	    google.maps.event.addDomListener(controlUI, 'click', this.deleteSelectedShape);
	}

	
	/**
	 *	Delete the drawed polygon  
	 **/
	this.deleteSelectedShape = function() {
    	  if(polygon){
    		  polygon.setMap(null);
    		  polygon = null;
    		  $rootScope.$apply(function () { 
		    		area = 0;
    		  });
    		  drawingManager.setOptions({
				  drawingControl: true
			});
    	  }
      }
	
	/**
	 * Draw a polygon, based on it's coordinates
	 */
	this.drawPolygon = function(coordinates){
		// polygon coordinates
		var coords = [];
		
		// checks if the polygon has at least 3 coordinates
		if(coordinates !== null && coordinates.length >= 3){
			// inserts then in the array
			for(var i = 0; i < coordinates.length; i++){
				coords.push({
					'lat' : coordinates[i].lat,
					'lng' : coordinates[i].lng
				});
			}
			
			coords.push({
				'lat' : coordinates[0].lat,
				'lng' : coordinates[0].lng
			});
			
			// Construct the polygon.			
			var params = polygonColor;
			params.paths = coords;
			
			var farmPolygon = new google.maps.Polygon(params);
			
			farmPolygon.setMap(map);
			map.fitBounds(farmPolygon.getBounds());
		}
	}
	
	
	
	/**
	 * Creates a heat map from heatMapData array
	 * Includes over each vertex a marker
	 */
	this.heatMap = function(heatMapData, markers){

		// Clean map from old heat map data
		if(heatMapData === null && heatmap){
			heatmap.setMap(null);
		}
		else{
			// Remove previous heat map
			if(heatmap){
				heatmap.setMap(null);
			}
			
			// Remove previous markers and infoWindows
			if(pointMarker.length){
				// Remove markers and infowindows
				for(i = 0; i < pointMarker.length; i++){
					pointMarker[i].setMap(null);
				}
				pointMarker = [];
			}
			
			// Creates a new heatmap
			heatmap = new google.maps.visualization.HeatmapLayer({
			  data: heatMapData,
			  radius: 60,
			  map : map
			});

			for (i = 0; i < markers.length; i++) {  
				pointMarker.push(new google.maps.Marker({
			         position: new google.maps.LatLng(markers[i][1], markers[i][2]),
			         map: map,
			         icon: "../resources/img/cornw.png",
			         title: markers[i][0] + '\n' + markers[i][3]
			    }));
			
				pointMarker[i].infowindow = new google.maps.InfoWindow({
				    content: markers[i][0] + '<br/>' + markers[i][3]
				  });
				
				//pointMarker[i].infowindow.open(map, pointMarker[i]);

				pointMarker[i].addListener('click', function(key) {
					return function() {
						pointMarker[key].infowindow.open(map, pointMarker[key]);
					}
				}(i));
			}
		}
	}
	
	
	/**
	 * Set map center based on a polygon
	 * @returns {google.maps.LatLngBounds}
	 */
	google.maps.Polygon.prototype.getBounds = function() {
	    var bounds = new google.maps.LatLngBounds();
	    var paths = this.getPaths();
	    var path;        
	    for (var i = 0; i < paths.getLength(); i++) {
	        path = paths.getAt(i);
	        for (var ii = 0; ii < path.getLength(); ii++) {
	            bounds.extend(path.getAt(ii));
	        }
	    }
	    return bounds;
	}
	
}]); 


