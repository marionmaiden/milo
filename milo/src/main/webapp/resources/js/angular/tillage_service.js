app.service('TillageService',['$http','$q', '$rootScope', function($http,$q, $rootScope){
	
	this.errorMessage = {text: "Erro ao acessar o servidor",
			   			 style: "alert alert-dismissible alert-warning",
			   			 messageType: "ERROR"};
	
	// Inserts a new Farm in the tillage
	this.updateFarm = function(tillage, farmId, ownerId){
		var deferred = $q.defer();
		
		tillage.farm = {id : farmId};
		tillage.ownerList = [{id : ownerId}];
		
		parameter = JSON.stringify(tillage);
		
		$http.post('/milo/rest/tillage/update', parameter).then(
			function(response) {
				deferred.resolve(response.data);
			},
			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
		return deferred.promise;
	}
	
	// Lists the Tillages by Owner ID
	this.listTillage = function(ownerId){
		var deferred = $q.defer();
		
		var parameter = {
			id: ownerId
		};
			
		parameter = JSON.stringify(parameter);
		
		$http.post('/milo/rest/tillage/list_by_owner',parameter).then(
			function(response) {
				deferred.resolve(response.data);
			},
			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
        return deferred.promise;
	}
	
	
	// Delete tillage by id
	this.removeTillage = function(idTillage){
		var deferred = $q.defer();
		
		$http.post('/milo/rest/tillage/delete',idTillage).then(
			function(response){
				deferred.resolve(response.data);
			},
			function(response){
				deferred.reject("");
			}
		);
		return deferred.promise;
	}
	
}]); 