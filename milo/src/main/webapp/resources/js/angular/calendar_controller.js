app.controller('CalendarController', ['$scope', 'CalendarService', 'UserFactory', 
                                      function($scope, CalendarService, UserFactory){
	
	// Variables for server messages
	$scope.msg = "";
	$scope.style = "";
	$scope.modalmsg = "";
	$scope.modalstyle = "";
	
	// Variables for new event modal
	$scope.text = "";
	$scope.date = "";
	
	$scope.calendars = [];
	
	$scope.datepicker = {
		dateFormat: 'yy-mm-dd',
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    nextText: 'Próximo',
	    prevText: 'Anterior'
	};
	
	$scope.clearData = function(){
		$scope.text = "";
		$scope.date = "";
		$scope.msg = "";
		$scope.style = "";
		$scope.modalmsg = "";
		$scope.modalstyle = "";
	}
	
	// Lists calendar events for a specific month
	$scope.list = function(){
		// Initialize Date
		if($scope.selectedDate === null){
			$scope.selectedDate = new Date();
		}
		
		// Clear calendar list
		$scope.calendars = [];
		$("#eventsDateLabel").html("Selecione uma data");
		
		UserFactory.getUser().then(
			function(result){
				CalendarService.listCalendar($scope.selectedDate, result).then(
					function(calendar){
						$scope.events = [];
						var mmt;
						
						for(i = 0; i < calendar.length; i++){
							mmt = moment(calendar[i].calendarDate);
							  
							$scope.events.push({
								moment: mmt,
								date: moment(calendar[i].calendarDate).format('DD/MM/YYYY'),
							    title: 'Evento',
							    content: calendar[i].text,
							    id: calendar[i].id
							});
						}
						$scope.initCalendar();
						
					},
					function(errorMessage){
						
				});
		});
	}

	// Saves a new event
	$scope.save = function(){

		if($scope.date === null || $scope.date === ""){
			$scope.modalstyle = "alert alert-dismissible alert-warning";
			$scope.modalmsg = "É necessário preencher a data do evento";
		}
		else if($scope.text === ""){
			$scope.modalstyle = "alert alert-dismissible alert-warning";
			$scope.modalmsg = "É necessário preencher a descrição do evento";
		}
		else{
			UserFactory.getUser().then(
				function(result){
			
					CalendarService.insertCalendar($scope.date, $scope.text, result).then(
						function(success){
							$('#calendarModal').modal('hide');
							$scope.style = success.style;
							$scope.msg = success.text;
							$scope.list();
							$scope.clearData();
						},
						function(fail){
							$scope.modalstyle = fail.style;
							$scope.modalmsg = fail.text;
						}
					);
			});
		}
	}
	
	
	// Removes an event
	$scope.remove = function(calendar){

		console.log(calendar);
		
		if(calendar.id === null){
			$scope.style = "alert alert-dismissible alert-danger";
			$scope.msg = "Falha na seleção do evento do calendário";
		}
		else{
			UserFactory.getUser().then(
				function(result){
			
					CalendarService.removeCalendar(calendar, result).then(
						function(success){
							$scope.style = success.style;
							$scope.msg = success.text;
							$scope.list();
						},
						function(fail){
							$scope.style = fail.style;
							$scope.msg = fail.text;
						}
					);
			});
		}
	}
	
	
	$scope.selectedDate = moment();
	$scope.monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
	$scope.dayNames = ["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
	
	$scope.events = [];
	
	$scope.initCalendar = function(){
		$('#events-calendar').empty();
		$('#events-calendar').bic_calendar(
    		{
    			date: $scope.selectedDate.format('DD[/]MM[/]YYYY'),
		        //list of events in array
		        events: $scope.events,
		        //enable select
		        enableSelect: true,
		        //enable multi-select
		        multiSelect: false,
		        //set day names
		        dayNames: $scope.dayNames,
		        //set month names
		        monthNames: $scope.monthNames,
		        //show dayNames
		        showDays: true,
		        //show month controller
		        displayMonthController: true,
		        //show year controller
		        displayYearController: true
    		}
    	);
    }
	
	$scope.eventListeners = function(){
		console.log("aqui");
		
		// Init datePicker
		$( "#date" ).datepicker($scope.datepicker);
		
		// Event: Change month or year in calendar
		document.addEventListener('bicCalendarChangeDate', $scope.changeDate);
	    
	    // Event: click on specific day on calendar
	    document.addEventListener('bicCalendarSelect', $scope.selectDate);
	}
	
	// Events implementation
	$scope.changeDate = function(e){
    	moment.locale('pt-BR');
    	$scope.selectedDate = new moment(Date.parse(e.detail.date));
    	$scope.calendars = [];
        $scope.list();
    }
	
	$scope.selectDate = function(e) {
    	moment.locale('pt-BR');
    	$scope.selectedDate = new moment(Date.parse(e.detail.date));

        $("#eventsDateLabel").html("Eventos para: " +  $scope.selectedDate.format('LL'));
        
        var day = $scope.selectedDate.format('DD');
        $scope.calendars = [];
        
        console.log($scope.events);
        
        for(i = 0; i < $scope.events.length; i++){
        	if($scope.events[i].moment.format('DD') === day){
        		$scope.$apply(function () { 
        			$scope.calendars.push($scope.events[i]);
        		});
        	}
        }
    }
	
}]);