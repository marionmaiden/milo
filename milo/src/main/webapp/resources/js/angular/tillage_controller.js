app.controller('TillageController', ['$scope','$rootScope', '$http', 'TillageService', 'FarmService', 'UserFactory', 
                                  function($scope, $rootScope, $http, TillageService, FarmService, UserFactory){
	
	// Variables for server messages
	$scope.msg = "";
	$scope.style = "";
	
	// Variables for server messages (modal)
	$scope.modalMsg = "";
	$scope.modalStyle = "";
	
	// tillage list
	$scope.tillages = "";
	
	// farm list
	$scope.farms = "";
	
	$scope.selectedTillage = "";
	$scope.selectedFarm = "";
	
	// Clears scope values
	$scope.clear = function(){
		$scope.msg = "";
		$scope.style = "";
	}
	
	// Associates a Tillage with Farm
	$scope.associateTillage =  function() {
		// Checks if some farm was selected
		if($scope.selectedFarm == "" ){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "É necessário selecionar uma Propriedade";
		}
		// Checks if some tillage was selected
		else if($scope.selectedTillage == ""){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "É necessário selecionar um Cultivar";
		}
		// Connects tillage with farm
		else{
			// Calls the service function
			UserFactory.getUser().then(
				function(result){
					
					var associate = TillageService.updateFarm($scope.selectedTillage, $scope.selectedFarm.id, result);
					
					associate.then(
							function(success){
								$scope.style = success.style;
								$scope.msg = success.text;
								$scope.list();
								$('#tillageModal').modal('hide');
							},
							function(fail){
								if(fail != undefined){
									$scope.style = fail.style;
									$scope.msg = fail.text;
								}
								else{
									$scope.style = "alert alert-dismissible alert-warning";
									$scope.msg = "Erro na comunicação com o servidor";
								}
								$('#tillageModal').modal('hide');
							}
					);
				});
		}
	}
	
	
	// List tillages
	$scope.list = function() {
		// Calls the service function
		UserFactory.getUser().then(
			function(result){
				var list = TillageService.listTillage(result);
				
				list.then(
						function(success){
							$scope.tillages = success;
						},
						function(fail){
							$scope.style = fail.style;
							$scope.msg = fail.text;
						}
				);

			});
	}
	
	
	// List farms
	$scope.listFarms = function(tillage) {
		
		// Set selected tillage
		$scope.selectedTillage = tillage;
		
		if($scope.farms === ""){
			// Calls the service function
			UserFactory.getUser().then(
				function(result){
					var list = FarmService.listFarm(result);
					
					list.then(
							function(success){
								$scope.farms = success;
							},
							function(fail){
								$scope.modalStyle = fail.style;
								$scope.modalMsg = fail.text;
							}
					);
	
				});
		}
	}
	
	
	// Removes a tillage
	$scope.remove = function(idTillage){
		if(idTillage === null || idTillage <= 0){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "Erro ao definir o Cultivar a ser removido";
		}
		else{
			bootbox.dialog({
				message: "Deseja remover o Cultivar?",
				title: "Confirmação",
				buttons: {
					main: {
						label: "Sim",
						className: "btn-primary",
						callback: function() {
							TillageService.removeTillage(idTillage).then(
								function(success){
									$scope.list();
									$scope.style = success.style;
									$scope.msg = success.text;
								},
								function(fail){
									$scope.style = fail.style;
									$scope.msg = fail.text;
								}
							);
						}
					},
					danger: {
						label: "Não",
						className: "btn-danger"
					}
				}
			});
		}
	}
	
}]);