app.service('FarmService',['$http','$q', '$rootScope', function($http,$q, $rootScope){
	
	this.errorMessage = {text: "Erro ao acessar o servidor",
			   			 style: "alert alert-dismissible alert-warning",
			   			 messageType: "ERROR"};
	
	
	// Inserts a new Farm in the server
	this.insertFarm = function(name, area, polygon, idUser){
		var deferred = $q.defer();
		
		var parameter = {
			name: name,
			area: area,
			polygon: polygon.getPath().getArray(),
			ownerList: [{id : idUser}]
		};
		
		parameter = JSON.stringify(parameter);
		
		$http.post('/milo/rest/farm/insert', parameter).then(
			function(response) {
				deferred.resolve(response.data);
			},
			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
		return deferred.promise;
	}
	
	
	// Lists the farms by Owner ID
	this.listFarm = function(ownerId){
		var deferred = $q.defer();
		
		var parameter = {
			id: ownerId
		};
			
		parameter = JSON.stringify(parameter);
		
		$http.post('/milo/rest/farm/list_by_owner',parameter).then(
			function(response) {
				deferred.resolve(response.data);
			},
			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
        return deferred.promise;
	}
	
	
	// Lists the farms by Tillage and Owner ID
	this.listFarmWithTillage = function(ownerId){
		var deferred = $q.defer();
		
		var parameter = {
			id: ownerId
		};
			
		parameter = JSON.stringify(parameter);
		
		$http.post('/milo/rest/farm/list_by_tillage',parameter).then(
			function(response) {
				deferred.resolve(response.data);
			},
			function(response) {
				deferred.reject(this.errorMessage);
			}
		);
        return deferred.promise;
	}
	
	
	// Delete farm by id
	this.removeFarm = function(idFarm){
		var deferred = $q.defer();
		
		var parameter = {
			id: idFarm
		};
		
		parameter = JSON.stringify(parameter);
		
		$http.post('/milo/rest/farm/delete',idFarm).then(
			function(response){
				deferred.resolve(response.data);
			},
			function(response){
				deferred.reject("");
			}
		);
		return deferred.promise;
	}
	
}]); 