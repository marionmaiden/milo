app.factory('AnalysisService', function(){
	
	var analyzedFarm = {};
	
	function set(farm){
		analyzedFarm = farm;
	}
	
	function get(){
		return analyzedFarm;
	}
	
	return{
		set: set,
		get: get
	}
	
});