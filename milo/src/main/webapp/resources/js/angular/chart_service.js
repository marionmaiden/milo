app.service('ChartService', function(){
	
	this.labels = [];
	this.series = [];
	
	
	this.initGraph = function(labels, series) {
		google.charts.setOnLoadCallback(drawChart);
      }
	
	
	 function drawChart(){
		 // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Mushrooms', 3],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]
        ]);

        // Set chart options
        var options = {'title':'How Much Pizza I Ate Last Night',
                       'width':400,
                       'height':300};

        var component = document.getElementById("ct-chart");
        
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(component);
        chart.draw(data, options);
	}

});