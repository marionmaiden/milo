var app = angular.module("miloApp", [ 'ngSanitize', 'ngRoute' ]);

app.config(['$routeProvider', '$httpProvider', //'$locationProvider',
    function($routeProvider, $httpProvider//, $locationProvider
    		) {

	//$locationProvider.html5Mode(true);

	$httpProvider.interceptors.push('responseObserver');
	
	$routeProvider
	.when('/', {
		templateUrl : 'main_content.html'//,
		//controller : 'HomeCtrl',
	})

	.when('/calendar_show', {
		templateUrl : 'calendar_show.html',
		controller : 'CalendarController',
	})

	.when('/farm_analyze', {
		templateUrl : 'farm_analyze.html',
		controller : 'AnalysisController',
	})
	
	.when('/farm_list', {
		templateUrl : 'farm_list.html',
		controller : 'FarmController',
	})
	
	.when('/farm_new', {
		templateUrl : 'farm_new.html',
		controller : 'FarmController',
	})
	
	.when('/tillage_import_list', {
		templateUrl : 'tillage_import_list.html',
		controller : 'TillageController',
	})
	
	.when('/tillage_list', {
		templateUrl : 'tillage_list.html',
		controller : 'FarmController',
	})
	
	.otherwise({
		redirectTo : '/'
//		redirectTo : function() {
//	        window.location = "../login/login.html";
//	    }
	});
}]);


app.factory('responseObserver', function responseObserver($q, $window) {
    return {
        'responseError': function(errorResponse) {
            switch (errorResponse.status) {
            case 403:
            	//alert('403');
                $window.location = '../login/login.html';
                break;
            case 404:
            	alert('404');
                $window.location = '../login/login.html';
                break;
            }
            return $q.reject(errorResponse);
        }
    };
});