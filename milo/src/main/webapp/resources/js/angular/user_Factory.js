app.factory('UserFactory', [ '$q', '$http', function($q, $http) {
	
	var loggedUser = null;
	
	// Returns the logged user
	function getUser() {
		var deferred = $q.defer();
		
		// If loggedUser has already been set
		if(loggedUser !== null){
			deferred.resolve(loggedUser);
		}
		// Else, checks on server loggedUser id
		else{
			$http.get('/milo/login/getUser', {})
				.then(function(result){
		        	if (result.status == 200) {
		        		loggedUser = result.data.text;
		        		deferred.resolve(loggedUser);
		            }
		            else {
		            	loggedUser = null;
		            	deferred.reject(loggedUser);
		            }
				}
			);
		}
		return deferred.promise;
	}
		
	
	return {
		getUser : getUser
	 }
}]);
