app.controller('FarmController', ['$scope','$rootScope', '$http', '$window' , 'FarmService', 'UserFactory', 'AnalysisService', 'MapService', 
                                  function($scope, $rootScope, $http, $window, FarmService, UserFactory, AnalysisService, MapService){
	
	// Variables for server messages
	$scope.msg = "";
	$scope.style = "";
	
	$scope.name = "";
	
	// Farm list
	$scope.farms = "";
	
	$scope.area = function(){
		return MapService.getArea();
	}
	
	$scope.polygon = function(){
		return MapService.getPolygon();
	}
	
	$scope.initializeMap = function() {
		MapService.initializeMap(true, true);
	}
	
	// Clears scope values
	$scope.clear = function(){
		$scope.msg = "";
		$scope.style = "";
		
		$scope.name = "";
		MapService.deleteSelectedShape();
		//$scope.deleteSelectedShape();
		MapService.area = null;
		//$scope.area = null;
	}
	
	// Saves a Farm
	$scope.save =  function() {
		// Checks if the farm name was set
		if($scope.name == "" ){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "É necessário preencher o nome da propriedade";
		}
		// Checks if the polygon was drawn
		else if($scope.polygon() === null || !$scope.polygon().getPath().getArray()){
		//else if($scope.polygon === null || !$scope.polygon.getPath().getArray()){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "É necessário desenhar a área da propriedade";
		}
		// Inserts the farm
		else{
			// Calls the service function
			UserFactory.getUser().then(
				function(result){
					var insert = FarmService.insertFarm($scope.name, $scope.area(), $scope.polygon(), result);
					//var insert = FarmService.insertFarm($scope.name, $scope.area, $scope.polygon, result);
					
					insert.then(
							function(success){
								$scope.style = success.style;
								$scope.msg = success.text;
							},
							function(fail){
								$scope.style = fail.style;
								$scope.msg = fail.text;
							}
					);
				});
		}
	}
	
	
	// List farms
	$scope.list = function() {
		// Calls the service function
		UserFactory.getUser().then(
			function(result){
				var list = FarmService.listFarm(result);
				
				list.then(
						function(success){
							$scope.farms = success;
						},
						function(fail){
							$scope.style = fail.style;
							$scope.msg = fail.text;
						}
				);
			});
	}
	
	
	// List farms with tillage
	$scope.listWithTillage = function() {
		// Calls the service function
		UserFactory.getUser().then(
			function(result){
				var list = FarmService.listFarmWithTillage(result);
				
				list.then(
						function(success){
							$scope.farms = success;
						},
						function(fail){
							$scope.style = fail.style;
							$scope.msg = fail.text;
						}
				);

			});
	}
	
	
	// Removes a farm
	$scope.remove = function(idFarm){
		if(idFarm === null || idFarm <= 0){
			$scope.style = "alert alert-dismissible alert-warning";
			$scope.msg = "Erro ao definir a Propriedade a ser removida";
		}
		else{
			bootbox.dialog({
				message: "Deseja remover a Propriedade?",
				title: "Confirmação",
				buttons: {
					main: {
						label: "Sim",
						className: "btn-primary",
						callback: function() {
							FarmService.removeFarm(idFarm).then(
								function(success){
									$scope.list();
									$scope.style = success.style;
									$scope.msg = success.text;
								},
								function(fail){
									$scope.style = fail.style;
									$scope.msg = fail.text;
								}
							);
						}
					},
					danger: {
						label: "Não",
						className: "btn-danger"
					}
				}
			});
		}
	}

	// Set a farm for analysis and redirect to analysis page
	$scope.analyze =  function(selectedFarm){
		AnalysisService.set(selectedFarm);
		
		$window.location.href = '#/farm_analyze';
	}
	
	
}]);