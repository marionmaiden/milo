var area_map = {
	pos : null,
	map : null,
	drawingManager : null,
	selectedShape : null,
	polygon : null,
	area : null,
	perimeter : null,
	
	// Parameters for the map
	mapParams : {	
		zoom : 15,
		streetViewControl : false,
		mapTypeControl : false,
		mapTypeId : google.maps.MapTypeId.HYBRID
	},
	// Parameters for the polygon
	polygonParams : {
		fillColor : '#00CCCC',
		fillOpacity : 0.3,
		strokeWeight : 2,
		clickable : true,
		editable : true,
		zIndex: 1
    },
	
    /**
     * Initialize Google Maps
     */
	initializeMap : function() {
		// Initialize map on map_canvas div
		map = new google.maps.Map(document.getElementById("map_canvas"), this.mapParams);

		// If the browser supports geolocation
		if (navigator.geolocation) {
			this.setGeolocation();
		}
		else {
			map.setCenter(new google.maps.LatLng(37.7749295, -122.4194155));
		}
		
		// Enables drawing on Map
		this.setDrawing();
	},
	
	/**
	 * Sets geolocation for the map
	 * Centers the map based on user location
	 */
	setGeolocation : function() {
		navigator.geolocation.getCurrentPosition(function(position) {
			this.pos = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};

			/*var pinImage = new google.maps.MarkerImage(
		    	"http://labs.google.com/ridefinder/images/mm_20_green.png",
		        new google.maps.Size(16, 20),
		        new google.maps.Point(0,0),
		        new google.maps.Point(8, 20));*/
			
			var marker = new google.maps.Marker({
			    position: pos,
			    map: map,
			    //icon: pinImage,
			    title:"Você está aqui"
			});
			// Centers the map on position
			this.map.setCenter(this.pos);
		});
	},
	
	/**
	 * Enables map to make drawings to set the property limits
	 */
	setDrawing : function(){
		// Initializes the drawing manager
		this.drawingManager = new google.maps.drawing.DrawingManager({
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_LEFT,
				drawingModes: [google.maps.drawing.OverlayType.POLYGON]
			},
			polygonOptions: this.polygonParams
		});
		
		// Create a custom Div for the "delete" button
		var customControlDiv = document.createElement('div');
	    var customControl = this.CustomControl(customControlDiv);
	    customControlDiv.index = 1;
	    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(customControlDiv);
		
	    // Adds a event listener for the polygon
		google.maps.event.addListener(area_map.drawingManager, 'overlaycomplete', function(event) {
			// Polygon draw event
			if (event.type == google.maps.drawing.OverlayType.POLYGON) {
				// Polygon vertex editing event 
				google.maps.event.addListener(event.overlay.getPath(), 'insert_at', function(index, obj) {
					area_map.polygon = event.overlay;
					area_map.area = (google.maps.geometry.spherical.computeArea(area_map.polygon.getPath()) / 1000).toFixed(2);
					//map_obj.perimeter = (google.maps.geometry.spherical.computeLength(map_obj.polygon.getPath()) / 1000).toFixed(2);
					document.getElementById("area").textContent = area_map.area;
					//document.getElementById("perimeter").textContent = map_obj.perimeter;
			    });
				// Polygon edge editing event
			    google.maps.event.addListener(event.overlay.getPath(), 'set_at', function(index, obj) {
			    	area_map.polygon = event.overlay;
			    	area_map.area = (google.maps.geometry.spherical.computeArea(area_map.polygon.getPath()) / 1000).toFixed(2);
			    	//map_obj.perimeter = (google.maps.geometry.spherical.computeLength(map_obj.polygon.getPath()) / 1000).toFixed(2);
			    	document.getElementById("area").textContent = area_map.area;
			    	//document.getElementById("perimeter").textContent = map_obj.perimeter;
			    });
			    area_map.polygon = event.overlay;
			    area_map.area = (google.maps.geometry.spherical.computeArea(area_map.polygon.getPath()) / 1000).toFixed(2);
			    //map_obj.perimeter = (google.maps.geometry.spherical.computeLength(map_obj.polygon.getPath()) / 1000).toFixed(2);
			    document.getElementById("area").textContent = area_map.area;
			    //document.getElementById("perimeter").textContent = map_obj.perimeter;

				// Switch back to non-drawing mode after drawing a shape.
				area_map.drawingManager.setDrawingMode(null);
				area_map.drawingManager.setOptions({
					  drawingControl: false
				});

			  }
			});

		this.drawingManager.setMap(map);
	},
	
      /**
       * 
       */
      deleteSelectedShape : function() {
    	  if(area_map.polygon){
    		  area_map.polygon.setMap(null);
    		  area_map.polygon = null;
    		  area_map.drawingManager.setOptions({
				  drawingControl: true
			});
    	  }
      },
	
	
	/**
	 * Adds a div with "clear map" button 
	 * @param controlDiv
	 */
	CustomControl : function (controlDiv) {
	    // Set CSS for the control border
	    var controlUI = document.createElement('div');
	    controlUI.style.backgroundColor = 'white';
	    controlUI.style.height = '24px';
	    controlUI.style.marginTop = '6px';
	    controlUI.style.marginRight = '6px';
	    controlUI.style.cursor = 'default';
	    controlUI.style.borderRadius="2px";
	    controlUI.title = 'Limpar mapa';

	    controlUI.onmouseover = function() {
	        this.style.backgroundColor = "Gainsboro";
	    }
	    controlUI.onmouseout = function() {
	        this.style.backgroundColor = "white";
	    }
	    
	    controlDiv.appendChild(controlUI);

	    // Set CSS for the control interior
	    var controlText = document.createElement('div');
	    controlText.style.fontSize = "16px";
	    controlText.style.paddingLeft = '6px';
	    controlText.style.paddingRight = '6px';
	    controlText.style.paddingTop = '4px';
	    controlText.innerHTML = '<i class="fa fa-trash"></i>';
	    controlUI.appendChild(controlText);

	    // Setup the click event listeners
	    google.maps.event.addDomListener(controlUI, 'click', this.deleteSelectedShape);
	}
};