/**
 * 
 */
var event_calendar = {
	selectedDate : null,
	monthNames : ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
    dayNames : ["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"],
    events : [
        {
        date: "28/12/2015",
        title: 'SPORT & WELLNESS',
        link: 'http://bic.cat',
        linkTarget: '_blank',
        color: '',
        content: '<\img src="http://gettingcontacts.com/upload/jornadas/sport-wellness_portada.png" ><\br>06-11-2013 - 09:00 <\br> Tecnocampus Mataró Auditori',
        class: '',
        displayMonthController: true,
        displayYearController: true,
        nMonths: 6
        }
    ],

    initCalendar : function(){
	    
	    $('#events-calendar').bic_calendar(
    		{
		        //list of events in array
		        events: this.events,
		        //enable select
		        enableSelect: true,
		        //enable multi-select
		        multiSelect: false,
		        //set day names
		        dayNames: this.dayNames,
		        //set month names
		        monthNames: this.monthNames,
		        //show dayNames
		        showDays: true,
		        //show month controller
		        displayMonthController: true,
		        //show year controller
		        displayYearController: true
    		}
    	);
	    
	    document.addEventListener('bicCalendarSelect', function(e) {
	    	moment.locale('pt-BR');
            this.selectedDate = new moment(e.detail.date);

            $("#eventsDateLabel").html("Eventos para: " +  this.selectedDate.format('LL'));
            
            //console.log("Selecionou: " +  dateFirst.format('YYYY[-]MM[-]DD'));
        });
    }
};