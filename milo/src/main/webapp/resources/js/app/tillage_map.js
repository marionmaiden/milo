var tillage_map = {
	pos : null,
	map : null,
	markers : [],
	
	// Parameters for the map
	mapParams : {	
		zoom : 15,
		streetViewControl : false,
		mapTypeControl : false,
		mapTypeId : google.maps.MapTypeId.HYBRID
	},
	
    /**
     * Initialize Google Maps
     */
	initializeMap : function() {
		// Initialize map on map_canvas div
		map = new google.maps.Map(document.getElementById("map_canvas"), this.mapParams);

		// If the browser supports geolocation
		//if (navigator.geolocation) {
		//	this.setGeolocation();
		//}
		//else {
			map.setCenter(new google.maps.LatLng(37.7749295, -122.4194155));
		//}
	},
	
	/**
	 * Sets geolocation for the map
	 * Centers the map based on user location
	 */
	setGeolocation : function() {
		navigator.geolocation.getCurrentPosition(function(position) {
			this.pos = {
				lat : position.coords.latitude,
				lng : position.coords.longitude
			};
			
			var marker = new google.maps.Marker({
			    position: pos,
			    map: map,
			    title:"Você está aqui"
			});
			// Centers the map on position
			this.map.setCenter(this.pos);
		});
	}
	
};